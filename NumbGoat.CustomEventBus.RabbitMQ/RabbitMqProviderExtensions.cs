﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace NumbGoat.CustomEventBus.RabbitMQ;

public static class RabbitMqProviderExtensions
{
    public static void AddRabbitMqProvider(IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<RabbitMqEventProvider>();
    }

    public static void AddRabbitMqOptions(IHostBuilder builder, string configurationPath = "RabbitMq")
    {
        builder.ConfigureServices((context, services) =>
        {
            services.Configure<RabbitMqOptions>(context.Configuration.GetSection(configurationPath));
        });
    }
}