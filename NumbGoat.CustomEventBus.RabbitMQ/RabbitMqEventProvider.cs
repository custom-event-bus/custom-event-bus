﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Exceptions;
using NumbGoat.CustomEventBus.Common.Proto;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace NumbGoat.CustomEventBus.RabbitMQ;

public class RabbitMqEventProvider : BaseProtoEventBusProvider
{
    private readonly ILogger<RabbitMqEventProvider> _logger;
    private readonly ConcurrentQueue<ProtoMessage> _outgoingMessages = new();
    private readonly RabbitMqOptions _rabbitMqOptions;
    private IModel? _channel;
    private IConnection? _connection;
    private EventingBasicConsumer? _queueConsumer;
    private Thread? _runThread;
    private EventingBasicConsumer? _topicConsumer;

    private bool _transportsCreated;

    public RabbitMqEventProvider(ILogger<RabbitMqEventProvider> logger, IOptions<RabbitMqOptions> rabbitMqOptions) : base(logger)
    {
        _logger = logger;
        _rabbitMqOptions = rabbitMqOptions.Value;
    }

    public override bool HasOutgoingMessages => !_outgoingMessages.IsEmpty;

    public override bool IsReady => _transportsCreated && (_connection?.IsOpen ?? false);

    public override bool SupportsQueues()
    {
        return true;
    }

    public override bool SupportsTopics()
    {
        return true;
    }

    public override async Task Start(IEnumerable<EventDefinition> eventTypes,
        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> registeredConsumers, CancellationToken stoppingToken)
    {
        IEnumerable<EventDefinition> eventDefinitions = eventTypes.ToList();
        await base.Start(eventDefinitions, registeredConsumers, stoppingToken);
        _runThread = new Thread(() =>
        {
            // Setup all the topics and queues.
            void CreateTopic(IModel connection, string topicName)
            {
                connection.ExchangeDeclare(topicName, ExchangeType.Fanout);
            }

            void CreateQueue(IModel connection, string queueName)
            {
                connection.QueueDeclare(queueName, true, false, false);
            }

            var factory = new ConnectionFactory
            {
                HostName = _rabbitMqOptions.Hostname,
                VirtualHost = _rabbitMqOptions.VirtualHost,
                UserName = _rabbitMqOptions.UserName,
                Password = _rabbitMqOptions.Password
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            string routingKeyPrefix = (string.IsNullOrWhiteSpace(_rabbitMqOptions.QueueNamePrefix)
                ? string.Empty
                : _rabbitMqOptions.QueueNamePrefix + "-") + _rabbitMqOptions.QueueNamePrefix;

            foreach (EventDefinition eventDefinition in eventDefinitions)
            {
                switch (eventDefinition.TransportType)
                {
                    case MessageTransportType.Queue:
                        string queueName = routingKeyPrefix + eventDefinition.TransportName;
                        CreateQueue(_channel, queueName);
                        ConsumeQueue(queueName);
                        break;
                    case MessageTransportType.Topic:
                        string exchangeName = routingKeyPrefix + eventDefinition.TransportName;
                        CreateTopic(_channel, exchangeName);
                        ConsumeTopic(exchangeName);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(eventDefinition.TransportType));
                }
            }

            _transportsCreated = true;
            Task runTask = Run(eventDefinitions, stoppingToken);
            runTask.Wait(CancellationToken.None);
        });
        _runThread.Start();
    }

    public void ConsumeQueue(string queueName)
    {
        if (_channel == null)
        {
            throw new NullReferenceException(nameof(_channel));
        }

        if (_queueConsumer == null)
        {
            _queueConsumer = new EventingBasicConsumer(_channel);
            _queueConsumer.Received += (sender, args) =>
            {
                Task t = QueueReceived(sender, args);
                t.Wait();
            };
        }

        _channel.BasicConsume(queueName, false, _queueConsumer);
    }


    public void ConsumeTopic(string exchangeName)
    {
        if (_channel == null)
        {
            throw new NullReferenceException(nameof(_channel));
        }

        if (_topicConsumer == null)
        {
            _topicConsumer = new EventingBasicConsumer(_channel);
            _topicConsumer.Received += (s, @event) =>
            {
                Task t = TopicReceived(s, @event);
                t.Wait();
            };
        }

        // Create queue bound to exchange for subscription.
        string? queueName = _channel.QueueDeclare().QueueName;
        _channel.QueueBind(queueName, exchangeName, "");
        _channel.BasicConsume(queueName, false, _topicConsumer);
    }

    private async Task QueueReceived(object? sender, BasicDeliverEventArgs e)
    {
        if (e == null)
        {
            throw new NullReferenceException(nameof(e));
        }

        await InvokeSubscriber(new IncomingProtoMessage(e.Body.ToArray(),
            e.DeliveryTag.ToString(), new MessageMetadata(e.RoutingKey, MessageTransportType.Queue)));
    }

    private async Task TopicReceived(object? sender, BasicDeliverEventArgs e)
    {
        if (e == null)
        {
            throw new NullReferenceException(nameof(e));
        }

        await InvokeSubscriber(new IncomingProtoMessage(e.Body.ToArray(), e.DeliveryTag.ToString(),
            new MessageMetadata(e.Exchange, MessageTransportType.Topic)));
    }

    private Task Run(IEnumerable<EventDefinition> eventTypes, CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            if (_outgoingMessages.IsEmpty)
            {
                SpinWait.SpinUntil(() => !_outgoingMessages.IsEmpty || stoppingToken.IsCancellationRequested);
                continue;
            }

            bool dequeueSuccess = _outgoingMessages.TryDequeue(out ProtoMessage? message);
            if (!dequeueSuccess)
            {
                _logger.LogWarning("Failed to dequeue message, trying again");
                continue;
            }

            if (message == null)
            {
                _logger.LogError("Got null message from queue");
                continue;
            }

            switch (message.Metadata.TransportType)
            {
                case MessageTransportType.Queue:
                    SendToQueue(message);
                    break;
                case MessageTransportType.Topic:
                    SendToTopic(message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        return Task.CompletedTask;
    }

    private void SendToQueue(ProtoMessage message)
    {
        if (_channel == null)
        {
            throw new NullReferenceException(nameof(_channel));
        }

        string routingKey = (string.IsNullOrWhiteSpace(_rabbitMqOptions.QueueNamePrefix)
            ? string.Empty
            : _rabbitMqOptions.QueueNamePrefix + "-") + message.Metadata.TransportName;
        IBasicProperties? props = _channel.CreateBasicProperties();
        _channel.BasicPublish(
            "",
            routingKey,
            false,
            props,
            message.MessageBytes
        );
    }

    private void SendToTopic(ProtoMessage message)
    {
        if (_channel == null)
        {
            throw new NullReferenceException(nameof(_channel));
        }

        string exchange = (string.IsNullOrWhiteSpace(_rabbitMqOptions.QueueNamePrefix)
            ? string.Empty
            : _rabbitMqOptions.QueueNamePrefix + "-") + message.Metadata.TransportName;

        IBasicProperties? props = _channel.CreateBasicProperties();
        _channel.BasicPublish(
            exchange,
            "",
            props,
            message.MessageBytes
        );
    }

    protected override Task PublishToQueue(ProtoMessage message)
    {
        _outgoingMessages.Enqueue(message);
        return Task.CompletedTask;
    }

    protected override Task PublishToTopic(ProtoMessage message)
    {
        if (message.Metadata.TransportType != MessageTransportType.Topic)
        {
            throw new InvalidMessageException($"PublishToTopic called with a non topic message {message}");
        }

        _outgoingMessages.Enqueue(message);
        return Task.CompletedTask;
    }

    protected override Task OnMessageComplete(IIncomingMessage completedMessage)
    {
        if (_channel == null)
        {
            throw new NullReferenceException(nameof(_channel));
        }

        if (completedMessage.MessageId == null)
        {
            throw new NullReferenceException("Completed messages must have valid id's");
        }

        ulong deliveryTag = ulong.Parse(completedMessage.MessageId);
        _channel.BasicAck(deliveryTag, true);
        return Task.CompletedTask;
    }

    protected override Task OnMessageFailure(IIncomingMessage failedMessage)
    {
        throw new NotImplementedException();
    }

    public Task ClearQueue(string queueName)
    {
        _channel?.QueueDelete(queueName);
        return Task.CompletedTask;
    }

    public Task ClearTopic(string topicName)
    {
        _channel?.ExchangeDelete(topicName);
        return Task.CompletedTask;
    }

    public override void Dispose()
    {
        base.Dispose();
        _channel?.Dispose();
        _connection?.Dispose();
    }
}