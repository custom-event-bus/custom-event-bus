﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.RabbitMQ;
using Serilog;
using Xunit.DependencyInjection.Logging;

namespace NumbGoat.CustomEventBus.RabbitMq.Tests;

public class Startup
{
    public IHostBuilder CreateHostBuilder()
    {
        if (Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") == null)
        {
            Environment.SetEnvironmentVariable("DOTNET_ENVIRONMENT", "Development");
        }

        return Host.CreateDefaultBuilder().ConfigureLogging(ConfigureLogging).ConfigureServices(ConfigureServices);
    }

    private void ConfigureLogging(HostBuilderContext context, ILoggingBuilder builder)
    {
        builder.AddSerilog();
    }

    private void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        services.AddLogging(lb => lb.AddXunitOutput());
        services.Configure<RabbitMqOptions>(context.Configuration.GetSection("RabbitMq"));
    }
}