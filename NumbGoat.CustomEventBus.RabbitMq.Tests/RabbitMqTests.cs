﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.RabbitMQ;
using NumbGoat.CustomEventBus.Tests.Common;
using NumbGoat.CustomEventBus.Tests.Common.Consumers;
using NumbGoat.CustomEventBus.Tests.Protos;
using Xunit;
using Xunit.Abstractions;

namespace NumbGoat.CustomEventBus.RabbitMq.Tests;

public class RabbitMqTests : IAsyncLifetime
{
    private readonly ILogger<RabbitMqTests> _logger;
    private readonly IConfiguration _configuration;
    private readonly ITestOutputHelper _outputHelper;
    private IHost? _host;
    private readonly CancellationTokenSource _cts;
    private RabbitMqEventProvider? _provider;


    public RabbitMqTests(ILogger<RabbitMqTests> logger, IConfiguration configuration, ITestOutputHelper outputHelper)
    {
        _logger = logger;
        _configuration = configuration;
        _outputHelper = outputHelper;
        _cts = new CancellationTokenSource();
    }

    public Task InitializeAsync()
    {
        HostHelpers.ResetAndRegisterTestEvents();
        return Task.CompletedTask;
    }

    public async Task DisposeAsync()
    {
        _cts?.Cancel();


        if (_provider != null)
        {
            await _provider.ClearQueue("test-event");
            await _provider.ClearTopic("test-event");
        }

        if (_host != null)
        {
            try
            {
                await _host.StopAsync();
            }
            catch (Exception)
            {
                // Ignore
            }
        }
    }

    private async Task<(CustomEventBus, RabbitMqEventProvider, TestSingleEventConsumer)> CreateEventBus()
    {
        (CustomEventBus bus, RabbitMqEventProvider provider, TestAutoEventConsumer consumer, IHost _) =
            await CreateEventBusHost();
        return (bus, provider, consumer);
    }

    [MemberNotNullWhen(true, nameof(_provider))]
    private async Task<(CustomEventBus, RabbitMqEventProvider, TestAutoEventConsumer, IHost)> CreateEventBusHost()
    {
        (IHost host, CustomEventBus customEventBus) =
            await HostHelpers.CreateBasicEventBus(new[] { typeof(RabbitMqEventProvider) },
                (context, collection) =>
                {
                    collection.Configure<RabbitMqOptions>(_configuration.GetSection("RabbitMq"));
                }, _outputHelper, configure: configuration => configuration.AutoLoadHandlers = true);
        _host = host;
        IEventBusProvider? foundProvider = host.Services.GetServices<IEventBusProvider>()
            .FirstOrDefault(p => p.GetType().IsAssignableTo(typeof(RabbitMqEventProvider)));
        _provider = foundProvider as RabbitMqEventProvider ??
                    throw new Exception("Could not get AzureServiceBus provider");
        var consumer = host.Services.GetRequiredService<TestAutoEventConsumer>();
        return (customEventBus, _provider, consumer, host);
    }

    private async Task<(CustomEventBus, RabbitMqEventProvider, TestSingleEventConsumer)> CreateAndStartEventBus()
    {
        (CustomEventBus customEventBus, RabbitMqEventProvider provider, TestSingleEventConsumer consumer, IHost host) =
            await CreateEventBusHost();
        await HostHelpers.StartEventBusAndWaitForReady(host);
        return (customEventBus, provider, consumer);
    }

    [Fact]
    public async Task CreateRabbitMqProvider_Success_00()
    {
        (_, RabbitMqEventProvider provider, _) = await CreateEventBus();
        provider.Should().NotBeNull();
    }

    [Fact]
    public async Task ConnectRabbitMqProvider_Success_00()
    {
        (_, RabbitMqEventProvider provider, _) = await CreateEventBus();
        await provider.Start(Array.Empty<EventDefinition>(), new List<KeyValuePair<Type, IEnumerable<Type>>>(),
            _cts.Token);
        SpinWait.SpinUntil(() => _provider is { IsReady: true }, TimeSpan.FromSeconds(10));
        provider.IsReady.Should().BeTrue();
    }

    [Fact]
    public async Task TestQueueMessageSend_Success_00()
    {
        (_, RabbitMqEventProvider provider, _) = await CreateEventBus();
        var events = new List<EventDefinition>
        {
            new(typeof(BasicTestEvent), "test-event", MessageTransportType.Queue)
        };
        await provider.Start(events, new List<KeyValuePair<Type, IEnumerable<Type>>>(), _cts.Token);
        var basicTestEvent = new BasicTestEvent
        {
            MessageText = "this is a test message"
        };
        var outgoingMessage = new OutgoingMessage(basicTestEvent, new MessageMetadata(
            "test-event", MessageTransportType.Queue));
        await provider.PublishToQueue(outgoingMessage);
        SpinWait.SpinUntil(() => !provider.HasOutgoingMessages, TimeSpan.FromSeconds(10));
        provider.HasOutgoingMessages.Should().BeFalse();
    }

    [Fact]
    public async Task TestTopicMessageSend_Success_00()
    {
        (_, RabbitMqEventProvider provider, _) = await CreateEventBus();
        var events = new List<EventDefinition>
        {
            new(typeof(BasicTestEvent), "test-event", MessageTransportType.Topic)
        };
        await provider.Start(events, new List<KeyValuePair<Type, IEnumerable<Type>>>(), _cts.Token);
        var basicTestEvent = new BasicTestEvent
        {
            MessageText = "this is a test message"
        };
        var outgoingMessage = new OutgoingMessage(basicTestEvent, new MessageMetadata(
            "test-event", MessageTransportType.Topic));
        await provider.PublishToTopic(outgoingMessage);
        SpinWait.SpinUntil(() => !provider.HasOutgoingMessages, TimeSpan.FromSeconds(10));
        provider.HasOutgoingMessages.Should().BeFalse();
    }

    [Fact]
    public async Task BasicTestEventBus_Success_00()
    {
        (CustomEventBus bus, RabbitMqEventProvider provider, TestSingleEventConsumer consumer) =
            await CreateAndStartEventBus();

        var message = new BasicTestEvent
        {
            MessageText = "this is a test message"
        };

        await bus.Publish(message);

        SpinWait.SpinUntil(() => _cts.IsCancellationRequested || !bus.HasOutgoingMessages, TimeSpan.FromSeconds(30));
        bus.HasOutgoingMessages.Should().BeFalse();
        SpinWait.SpinUntil(() => _cts.IsCancellationRequested || consumer.Count >= 1, TimeSpan.FromSeconds(30));
        consumer.Count.Should().Be(1);
    }
}