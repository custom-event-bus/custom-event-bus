using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Options;
using NumbGoat.CustomEventBus.Tests.Common.Consumers;
using NumbGoat.CustomEventBus.Tests.Protos;
using Xunit.Abstractions;

namespace NumbGoat.CustomEventBus.Tests.Common;

public static class HostHelpers
{
    public static Task<(IHost, CustomEventBus)> CreateBasicEventBus(
        IEnumerable<Type> providers, ITestOutputHelper? outputHelper = null,
        Action<CustomEventBusConfiguration>? configure = null)
    {
        return CreateBasicEventBus(providers, null, outputHelper: outputHelper, configure: null);
    }

    public static Task<(IHost, CustomEventBus)> CreateBasicEventBus(
        IEnumerable<Type> providers, Action<HostBuilderContext, IServiceCollection>? configureOptions = null,
        ITestOutputHelper? outputHelper = null, Action<CustomEventBusConfiguration>? configure = null)
    {
        return CreateBasicEventBus(providers, eventToConsumerMaps: null, configureOptions: configureOptions,
            outputHelper: outputHelper, configure: configure);
    }

    public static Task<(IHost, CustomEventBus)> CreateBasicEventBus(
        IEnumerable<Type> providers, IEnumerable<EventConsumerMapping>? eventToConsumerMaps = null,
        Action<HostBuilderContext, IServiceCollection>? configureOptions = null,
        ITestOutputHelper? outputHelper = null, Action<CustomEventBusConfiguration>? configure = null)
    {
        eventToConsumerMaps ??= new List<EventConsumerMapping>();

        IHostBuilder hostBuilder = Host.CreateDefaultBuilder()
            .ConfigureServices((context, services) =>
            {
                if (outputHelper != null)
                {
                    services.AddLogging(builder => builder.AddXUnit(outputHelper));
                }

                configureOptions?.Invoke(context, services);

                foreach (Type eventBusProviderType in providers)
                {
                    if (!eventBusProviderType.IsAssignableTo(typeof(IEventBusProvider)))
                    {
                        throw new Exception("Provider is not assignable to IEventBusProvider");
                    }

                    services.AddSingleton(typeof(IEventBusProvider), eventBusProviderType);
                }

                // Add consumers.
                services.AddSingleton<TestSingleEventConsumer>();
                services.AddSingleton<TestAutoEventConsumer>();
                services.AddSingleton<TestAutoMultiEventConsumer>();

                services.AddCustomEventBus(configuration =>
                {
                    configuration.AutoLoadHandlers = false;
                    configuration.ConsumerAssemblies.Add(Assembly.Load("NumbGoat.CustomEventBus.Tests.Common"));
                    configure?.Invoke(configuration);
                    configuration.ConsumerEventTypeMappings.AddRange(eventToConsumerMaps);
                });
            });
        IHost host = hostBuilder.Build();
        var eventBus = host.Services.GetRequiredService<CustomEventBus>();
        return Task.FromResult((host, eventBus));
    }

    public static async Task StartEventBusAndWaitForReady(IHost host, CancellationToken cancellationToken = default)
    {
        await host.StartAsync(cancellationToken);
        var eventBus = host.Services.GetRequiredService<CustomEventBus>();
        SpinWait.SpinUntil(() => eventBus is { IsReady: true, IsAcceptingNewMessages: true }, TimeSpan.FromMinutes(1));
    }

    public static void ResetAndRegisterTestEvents()
    {
        EventRegistry.Clear();
        EventRegistry.AddAllEvents("NumbGoat.CustomEventBus.Tests.Protos",
            Assembly.Load("NumbGoat.CustomEventBus.Tests.Protos"));
    }
}