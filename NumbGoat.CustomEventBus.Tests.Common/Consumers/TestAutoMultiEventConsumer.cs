using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Protobuf;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Attributes;
using NumbGoat.CustomEventBus.Tests.Protos;

namespace NumbGoat.CustomEventBus.Tests.Common.Consumers;

[EventHandler(typeof(BasicTestEvent))]
[EventHandler(typeof(BasicTestEventTwo))]
public class TestAutoMultiEventConsumer : BaseMultipleConsumer
{
    /// <summary>
    ///     Message received count
    /// </summary>
    public int CountOne { get; protected set; }

    /// <summary>
    ///     Message received count
    /// </summary>
    public int CountTwo { get; protected set; }

    /// <summary>
    /// Total number of messages received.
    /// </summary>
    public int OverallCount => CountOne + CountTwo;

    /// <summary>
    ///     Header received count
    /// </summary>
    public int HeaderCount { get; protected set; }

    /// <summary>
    ///     Messages text that have been received.
    /// </summary>
    public List<string> ReceivedMessagesOne { get; protected set; } = new();

    /// <summary>
    ///     Messages text that have been received.
    /// </summary>
    public List<string> ReceivedMessagesTwo { get; protected set; } = new();

    /// <summary>
    /// Custom header that have been received.
    /// </summary>
    public List<IMessage> CustomHeaders { get; protected set; } = new();

    /// <summary>
    ///     Reset counter to 0;
    /// </summary>
    public void Reset()
    {
        CountOne = 0;
        CountTwo = 0;
        ReceivedMessagesOne.Clear();
        ReceivedMessagesTwo.Clear();
        HeaderCount = 0;
        CustomHeaders.Clear();
    }

    public override Task OnHandle(IMessage message, MessageMetadata metadata, string? messageId,
        IMessage? customHeader = null)
    {
        switch (message)
        {
            case BasicTestEvent basicTestEvent:
                ReceivedMessagesOne.Add(basicTestEvent.MessageText);
                CountOne++;
                break;
            case BasicTestEventTwo basicTestEventTwo:
                ReceivedMessagesTwo.Add(basicTestEventTwo.MessageText);
                CountTwo++;
                break;
            default:
                throw new ArgumentOutOfRangeException(message.GetType().ToString());
        }

        if (customHeader != null)
        {
            CustomHeaders.Add(customHeader);
            HeaderCount++;
        }

        return Task.CompletedTask;
    }
}