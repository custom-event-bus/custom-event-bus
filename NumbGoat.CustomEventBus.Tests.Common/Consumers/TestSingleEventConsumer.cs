﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Protobuf;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Tests.Protos;

namespace NumbGoat.CustomEventBus.Tests.Common.Consumers;

/// <summary>
/// Test consumer for manually mapping consumer types.
/// </summary>
public class TestSingleEventConsumer : BaseSingleConsumer<BasicTestEvent>
{
    /// <summary>
    ///     Message received count
    /// </summary>
    public int Count { get; protected set; }
    
    /// <summary>
    ///     Header received count
    /// </summary>
    public int HeaderCount { get; protected set; }

    /// <summary>
    ///     Messages text that have been received.
    /// </summary>
    public List<string> ReceivedMessages { get; protected set; } = new();

    /// <summary>
    /// Custom header that have been received.
    /// </summary>
    public List<IMessage> CustomHeaders { get; protected set; } = new();

    /// <summary>
    ///     Reset counter to 0;
    /// </summary>
    public void Reset()
    {
        Count = 0;
        ReceivedMessages.Clear();
        HeaderCount = 0;
        CustomHeaders.Clear();
    }

    public override Task OnHandle(BasicTestEvent message, MessageMetadata metadata, string? messageId,
        IMessage? customHeader = null)
    {
        ReceivedMessages.Add(message.MessageText);
        if (customHeader != null)
        {
            CustomHeaders.Add(customHeader);
            HeaderCount++;
        }
        Count++;
        return Task.CompletedTask;
    }
}