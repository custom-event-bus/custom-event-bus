using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Protobuf;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Attributes;
using NumbGoat.CustomEventBus.Tests.Protos;

namespace NumbGoat.CustomEventBus.Tests.Common.Consumers;

[EventHandler]
public class TestAutoEventConsumer : TestSingleEventConsumer
{
}