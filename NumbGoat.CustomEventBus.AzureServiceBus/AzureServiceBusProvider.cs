using System.Collections.Concurrent;
using Azure.Messaging.ServiceBus;
using Azure.Messaging.ServiceBus.Administration;
using Google.Protobuf;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NumbGoat.CustomEventBus.AzureServiceBus.Exceptions;
using NumbGoat.CustomEventBus.AzureServiceBus.Options;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Exceptions;
using NumbGoat.CustomEventBus.Common.Proto;

namespace NumbGoat.CustomEventBus.AzureServiceBus;

public class AzureServiceBusProvider : BaseProtoEventBusProvider
{
    private readonly ConcurrentDictionary<string, ServiceBusReceivedMessage> _inFlightMessages = new();
    private readonly ILogger<BaseEventBusProvider> _logger;
    private readonly ConcurrentQueue<ProtoMessage> _outgoingMessages = new();
    private readonly ConcurrentDictionary<string, ServiceBusReceiver> _receivers = new();
    private readonly List<Thread> _receiverThreads = new();
    private readonly AutoResetEvent _resetEvent = new(true);
    private readonly ConcurrentDictionary<string, ServiceBusSender> _senders = new();
    private readonly ServiceBusAdministrationClient _serviceBusAdministration;
    private readonly ServiceBusClient _serviceBusClient;
    private readonly AzureServiceBusProviderOptions _serviceBusOptions;
    private bool _messageInFlight;
    private Thread? _runThread;
    private CancellationToken? _stoppingToken;

    private bool _transportsCreated;

    public AzureServiceBusProvider(ILogger<AzureServiceBusProvider> logger,
        IOptions<AzureServiceBusProviderOptions> serviceBusOptions)
        : base(logger)
    {
        _logger = logger;
        _serviceBusOptions = serviceBusOptions.Value;
        _serviceBusClient = new ServiceBusClient(_serviceBusOptions.ConnectionString, new ServiceBusClientOptions());
        _serviceBusAdministration =
            new ServiceBusAdministrationClient(_serviceBusOptions.ConnectionString,
                new ServiceBusAdministrationClientOptions());
    }

    public override bool IsReady => _transportsCreated && _runThread != null && !_serviceBusClient.IsClosed;
    public override bool HasOutgoingMessages => _outgoingMessages.Count > 0 || _messageInFlight;

    public override bool SupportsQueues()
    {
        return true;
    }

    public override bool SupportsTopics()
    {
        return true;
    }

    private bool GetLock()
    {
        bool success = _resetEvent.WaitOne(10000);
        return success;
    }

    private void ReleaseLock()
    {
        _resetEvent.Set();
    }


    public override async Task Start(IEnumerable<EventDefinition> eventTypes,
        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> registeredConsumers, CancellationToken stoppingToken)
    {
        if (_runThread != null)
        {
            _logger.LogError("AzureServiceBusProvider::Start called when its already running.");
            return;
        }

        _stoppingToken = stoppingToken;
        List<KeyValuePair<Type, IEnumerable<Type>>> consumers = registeredConsumers.ToList();
        IEnumerable<EventDefinition> eventDefinitions = eventTypes.ToList();
        await base.Start(eventDefinitions, consumers, stoppingToken);

        // Create our running thread and start it.
        async void ThreadStart()
        {
            List<EventDefinition> topics = eventDefinitions.Where(e => e.TransportType == MessageTransportType.Topic)
                .Distinct().ToList();
            List<EventDefinition> queues = eventDefinitions.Where(e => e.TransportType == MessageTransportType.Queue)
                .Distinct().ToList();

            // Setup work
            if (_serviceBusOptions.ShouldCreateQueues)
            {
                foreach (EventDefinition queue in queues)
                {
                    var queueName = $"{_serviceBusOptions.TransportPrefix}{queue.TransportName}";
                    if (!await _serviceBusAdministration.QueueExistsAsync(queueName, stoppingToken))
                    {
                        await _serviceBusAdministration.CreateQueueAsync(queueName, stoppingToken);
                    }
                }
            }

            if (_serviceBusOptions.ShouldCreateTopics)
            {
                foreach (EventDefinition topic in topics)
                {
                    var topicName = $"{_serviceBusOptions.TransportPrefix}{topic.TransportName}";
                    if (!await _serviceBusAdministration.TopicExistsAsync(topicName, stoppingToken))
                    {
                        await _serviceBusAdministration.CreateTopicAsync(topicName, stoppingToken);
                    }
                }
            }

            _transportsCreated = true;

            // Subscribe to events
            IEnumerable<Type> consumerEventTypes = consumers.Select(pair => pair.Key).Distinct();
            foreach (Type eventType in consumerEventTypes)
            {
                EventDefinition eventDefinition = EventRegistry.GetDefinition(eventType) ??
                                                  throw new InvalidEventException(
                                                      $"Failed to find event for {eventType.FullName}");
                await ActivateReceiver(eventDefinition);
            }

            // Run the main loop
            await Run(stoppingToken);
        }

        _runThread = new Thread(ThreadStart);

        _runThread.Start(); // Start the thread and return back to the service for normal activity
    }

    private async Task ActivateReceiver(EventDefinition eventDefinition)
    {
        ServiceBusReceiver? receiver = eventDefinition.TransportType switch
        {
            MessageTransportType.Queue => await GetReceiver(eventDefinition.TransportName),
            MessageTransportType.Topic => await GetReceiver(eventDefinition.TransportName,
                _serviceBusOptions.SubscriptionName),
            _ => throw new ArgumentOutOfRangeException(nameof(eventDefinition))
        };

        if (receiver == null)
        {
            throw new NullReferenceException("Failed to CreateReceiver");
        }

        if (_stoppingToken == null)
        {
            throw new NullReferenceException("Stopping token should not be null");
        }

        async void ReceiveThread()
        {
            try
            {
                while (!_stoppingToken.Value.IsCancellationRequested)
                {
                    ServiceBusReceivedMessage?
                        message = await receiver.ReceiveMessageAsync(null, _stoppingToken!.Value);
                    if (message == null)
                    {
                        continue;
                    }

                    _inFlightMessages.TryAdd(message.MessageId, message);

                    _logger.LogDebug("Received message {MessageId} which has been delivered {DeliveryCount} times",
                        message.MessageId, message.DeliveryCount);
                    await InvokeSubscriber(new IncomingProtoMessage(message.Body.ToArray(), message.MessageId,
                        new MessageMetadata(eventDefinition.TransportName, eventDefinition.TransportType)));
                    Thread.Sleep(200);
                }
            }
            catch (TaskCanceledException)
            {
            }
        }

        var receiverThread = new Thread(ReceiveThread);
        _receiverThreads.Add(receiverThread);
        receiverThread.Start();
    }

    private async Task Run(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            // Sleep until there is something to do.
            SpinWait.SpinUntil(() => HasOutgoingMessages || stoppingToken.IsCancellationRequested);

            // Get something from queue
            try
            {
                _messageInFlight = true;
                if (!_outgoingMessages.TryDequeue(out ProtoMessage? message))
                {
                    _logger.LogTrace("Failed to dequeue message from queue properly.");
                    continue;
                }


                if (message == null)
                {
                    throw new NullReferenceException("Failed to get message from send queue");
                }

                // Send the message
                ServiceBusSender? sender = GetSender(message.Metadata.TransportName);
                if (sender == null)
                {
                    throw new NullReferenceException($"Failed to get sender for {message.Metadata.TransportName}");
                }

                try
                {
                    await sender.SendMessageAsync(new ServiceBusMessage
                    {
                        ContentType = "application/octet-stream",
                        Body = BinaryData.FromBytes(message.MessageBytes)
                    }, CancellationToken.None);
                }
                catch (ServiceBusException e)
                {
                    if (!e.Message.Contains("cancelled"))
                    {
                        throw;
                    }
                }

                _logger.LogDebug("Sending message to transport {TransportName}", message.Metadata.TransportName);
            }
            finally
            {
                _messageInFlight = false;
            }
        }
    }

    /// <summary>
    ///     Gets a cached sender, or creates a new one and caches it.
    /// </summary>
    /// <param name="transportName">The topic or queue name for the sender.</param>
    /// <returns></returns>
    private ServiceBusSender? GetSender(string transportName)
    {
        if (_senders.ContainsKey(transportName))
        {
            return _senders[transportName];
        }

        ServiceBusSender? sender = _serviceBusClient.CreateSender(transportName);
        _senders.TryAdd(transportName, sender);
        return sender;
    }

    /// <summary>
    ///     Gets a cached receiver, or creates a new one and caches it.
    /// </summary>
    /// <param name="transportName">Name of the queue or topic to create receiver for.</param>
    /// <param name="subscriptionName">If transport is a topic this is the name of subscription to that topic. If not </param>
    /// <returns></returns>
    /// <exception cref="InvalidEventException"></exception>
    private async Task<ServiceBusReceiver?> GetReceiver(string transportName, string? subscriptionName = null)
    {
        if (subscriptionName != null && _serviceBusOptions.ShouldCreateTopicSubscriptions)
        {
            await EnsureSubscriptionExists(transportName, subscriptionName);
        }
        else if (subscriptionName != null)
        {
            if (!await SubscriptionExists(transportName, subscriptionName))
            {
                throw new InvalidEventException($"Subscription does not exist for {transportName}:{subscriptionName}");
            }
        }

        if (_receivers.ContainsKey(transportName))
        {
            bool success = _receivers.TryGetValue(transportName, out ServiceBusReceiver? existingReceiver);
            var attempts = 0;
            while (!success)
            {
                success = _receivers.TryGetValue(transportName, out existingReceiver);
                if (attempts++ > 3)
                {
                    throw new Exception("Failed to retrieve a receiver");
                }
            }

            return existingReceiver;
        }

        ServiceBusReceiver? receiver = subscriptionName != null
            ? _serviceBusClient.CreateReceiver(transportName, subscriptionName, new ServiceBusReceiverOptions
            {
                ReceiveMode = ServiceBusReceiveMode.PeekLock
            })
            : _serviceBusClient.CreateReceiver(transportName);
        _receivers.TryAdd(transportName, receiver);
        return receiver;
    }

    protected override Task PublishToQueue(ProtoMessage protoMessage)
    {
        // Queue up and handle in our thread.
        _outgoingMessages.Enqueue(protoMessage);
        return Task.CompletedTask;
    }

    protected override Task PublishToTopic(ProtoMessage protoMessage)
    {
        // Queue up and handle in our thread.
        _outgoingMessages.Enqueue(protoMessage);
        return Task.CompletedTask;
    }

    protected override async Task OnMessageComplete(IIncomingMessage completedMessage)
    {
        if (completedMessage.MessageId == null)
        {
            throw new NullReferenceException(nameof(completedMessage.MessageId));
        }

        if (_inFlightMessages.ContainsKey(completedMessage.MessageId))
        {
            bool success = _inFlightMessages.TryGetValue(completedMessage.MessageId,
                out ServiceBusReceivedMessage serviceBusMessage);
            if (!success)
            {
                _logger.LogError("Failed to retrieve inflight message for {MessageId}, was it handled elsewhere?",
                    completedMessage.MessageId);
            }

            if (_receivers.TryGetValue(completedMessage.Metadata.TransportName, out ServiceBusReceiver? receiver))
            {
                await receiver.CompleteMessageAsync(serviceBusMessage);
                _logger.LogDebug("Completed message {MessageId}", serviceBusMessage?.MessageId);
            }
        }
        else
        {
            throw new Exception("Failed to find MessageId for a completed message");
        }
    }

    protected override Task OnMessageFailure(IIncomingMessage failedMessage)
    {
        if (failedMessage.MessageId == null)
        {
            throw new NullReferenceException(nameof(failedMessage.MessageId));
        }

        if (!_inFlightMessages.ContainsKey(failedMessage.MessageId))
        {
            return Task.CompletedTask;
        }

        bool success = _inFlightMessages.TryGetValue(failedMessage.MessageId,
            out ServiceBusReceivedMessage serviceBusMessage);
        if (!success)
        {
            _logger.LogError("Failed to retrieve inflight message for {MessageId}, was it handled elsewhere?",
                failedMessage.MessageId);
        }

        if (_receivers.TryGetValue(failedMessage.Metadata.TransportName, out ServiceBusReceiver? receiver))
        {
            receiver.AbandonMessageAsync(serviceBusMessage);
        }

        return Task.CompletedTask;
    }

    /// <summary>
    ///     Completely empties a transport, discarding all messages.
    /// </summary>
    public async Task ClearTransport(string transportName, string? subscriptionName = null)
    {
        ServiceBusReceiver? receiver = await GetReceiver(transportName, subscriptionName);
        if (receiver == null)
        {
            return;
        }

        ServiceBusReceivedMessage? message = await receiver.ReceiveMessageAsync(TimeSpan.FromMilliseconds(300));
        while (message != null)
        {
            await receiver.CompleteMessageAsync(message);
            message = await receiver.ReceiveMessageAsync(TimeSpan.FromMilliseconds(300));
        }
    }

    public override void Dispose()
    {
        base.Dispose();
        ValueTask disposeTask = _serviceBusClient.DisposeAsync();
        disposeTask.AsTask().Wait();
    }

    /// <summary>
    ///     Checks if a subscription exists, and if not creates it.
    /// </summary>
    internal async Task EnsureSubscriptionExists(string topicName, string subscriptionName)
    {
        bool gotLock = GetLock();
        if (!gotLock)
        {
            throw new LockFailedException("EnsureSubscriptionExists failed to get lock");
        }

        try
        {
            if (!await SubscriptionExists(topicName, subscriptionName))
            {
                try
                {
                    await _serviceBusAdministration.CreateSubscriptionAsync(
                        new CreateSubscriptionOptions(topicName, subscriptionName)
                        {
                            AutoDeleteOnIdle = TimeSpan.FromDays(_serviceBusOptions.TransportCreationOptions.AutoDeleteDays)
                        });
                }
                catch (ServiceBusException serviceBusException)
                {
                    // We will ignore this exception if it is because the subscription already exists, because that is what we want.
                    // This seems to be an issue with the async nature of creating subscriptions. You requests one to be created,
                    // it gets created, but takes a little time for it to come back as existing.
                    if (serviceBusException.Reason != ServiceBusFailureReason.MessagingEntityAlreadyExists)
                    {
                        throw;
                    }
                }
            }
        }
        finally
        {
            if (gotLock)
            {
                ReleaseLock();
            }
        }
    }

    /// <summary>
    ///     Checks if a subscription exists
    /// </summary>
    /// <returns></returns>
    internal async Task<bool> SubscriptionExists(string topicName, string subscriptionName)
    {
        if (!await _serviceBusAdministration.TopicExistsAsync(topicName))
        {
            return false;
        }

        return await _serviceBusAdministration.SubscriptionExistsAsync(topicName, subscriptionName);
    }

    /// <summary>
    ///     Removes a subscription. If it does not exist, does nothing.
    /// </summary>
    /// <param name="topicName"></param>
    /// <param name="subscriptionName"></param>
    internal async Task RemoveSubscription(string topicName, string subscriptionName)
    {
        if (await SubscriptionExists(topicName, subscriptionName))
        {
            bool gotLock = GetLock();
            try
            {
                await _serviceBusAdministration.DeleteSubscriptionAsync(topicName, subscriptionName);
            }
            finally
            {
                if (gotLock)
                {
                    ReleaseLock();
                }
            }
        }
    }
}