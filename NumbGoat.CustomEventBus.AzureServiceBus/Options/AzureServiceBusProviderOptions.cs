namespace NumbGoat.CustomEventBus.AzureServiceBus.Options;

public class AzureServiceBusProviderOptions
{
    /// <summary>
    ///     Connection string for Azure Service Bus
    /// </summary>
    public string ConnectionString { get; set; } = null!;

    /// <summary>
    ///     Prefix for transport names etc
    /// </summary>
    public string? TransportPrefix { get; set; }

    /// <summary>
    ///     Name for topic subscriptions
    /// </summary>
    public string? SubscriptionName { get; set; } = Environment.MachineName;

    /// <summary>
    ///     Should queues be created automatically, or only use existing queues.
    ///     Default: false
    /// </summary>
    public bool ShouldCreateQueues { get; set; } = false;

    /// <summary>
    ///     Should topics be created automatically, or only use existing topics.
    ///     Default: false
    /// </summary>
    public bool ShouldCreateTopics { get; set; } = false;

    /// <summary>
    ///     Should subscriptions to topics be create automatically, or only use existing subscriptions.
    ///     If true, subscriptions are only created as needed.
    ///     Default: true
    /// </summary>
    public bool ShouldCreateTopicSubscriptions { get; set; } = true;

    /// <summary>
    ///     Options to use when creating a new queue (due to ShouldCreateQueues etc being true)
    /// </summary>
    public AzureTransportCreationOptions TransportCreationOptions { get; set; } = new();
}

public class AzureTransportCreationOptions
{
    public int MessageTtl { get; set; } = 300;
    public int AutoDeleteDays { get; set; } = 7;
}