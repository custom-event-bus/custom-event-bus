# Custom Event Bus: Azure Service Bus Provider

This is a CustomEventBus provider that uses the Azure Service Bus to send/receive messages.

# Configuration

To setup and use the provider we need to configure the options `AzureServiceBusProviderOptions` in the DI solution
before injecting the provider.

```csharp
// Load the configuration from the relevant configuration section.
services.Configure<AzureServiceBusProviderOptions>(context.Configuration.GetSection("AzureServiceBus"));
// Inject the provider
services.AddSingleton<IEventBusProvider, AzureServiceBusProvider>();
```