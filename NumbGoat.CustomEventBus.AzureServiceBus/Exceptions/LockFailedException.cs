namespace NumbGoat.CustomEventBus.AzureServiceBus.Exceptions;

public class LockFailedException : Exception
{
    public LockFailedException(string message) : base(message)
    {
    }

    public LockFailedException(string message, Exception innerException) : base(message, innerException)
    {
    }
}