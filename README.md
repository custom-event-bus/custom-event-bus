# Custom Event Bus

Custom Event Bus is a lightweight wrapper around many popular event bus / messaging services.
It uses Protocol Buffers as a serialization mechanism to allow for good message size and serialization speed.

# Usage

Custom Event Bus is built with the .Net 5/6 DI in mind but should work with any DI system that injects via constructor.

### Register Events

First we need to setup the Event Registry with our events. We can configure our events one-by-one like so:

```csharp
EventRegistry.RegisterEvent(new EventDefinition(typeof(TestEvent), "test-queue", MessageTransportType.Queue));
```

This registers `TestEvent` as a event type, to be put on the transport of name `test-queue"`, which is of type `Queue`.   
We can also auto register events by calling

```csharp
EventRegistry.AddAllEvents("NumbGoat.CustomEventBus.Tests.Protos", Assembly.Load("NumbGoat.CustomEventBus.Tests"));
```
This loads the Assembly `NumbGoat.CustomEventBus.Tests`, and loads all events that start with (contained in
namespace) `NumbGoat.CustomEventBus.Tests.Protos`. The autoloader will generate transport names that are the kebab-case
version of the class name.
Events that ends with `Job` will become a queue, everything else will be a topic. If an event is manually registered
before this call it is skipped.

### Provider

Next we need to create our provider of choice. A provider is anything that implements `IEventBusProvider` and is the
implementation of a external service bus like system that provides topic and/or queues for us to use.
The currently main supported providers are

- RabbitMQ
- Azure Service Bus

but more can be added at anytime. There is also the InMemoryProvider which can be used for testing or proof of concepts,
but is limited to the current process for communication so has no use in a real application.   
Once we have chosen our provider we need to inject this into the Dependency Injection system like so;

```csharp
services.AddSingleton<IEventBusProvider, InMemoryProvider>();
```

Some providers require specific configuration so refer to their specific README to get started.

Custom event bus supports having multiple provider types registered. In this case messages are sent to all providers 
when they are published.

### Configuration

Aside from any configuration that the providers require, there is also some basic configuration required for
CustomEventBus.
We can handle adding this configuration and registering CustomEventBus with the DI solution in one step by
calling `AddCustomEventBus` on any `IServiceCollection`. We can then configure the options section like so:

```csharp
services.AddCustomEventBus(configuration =>
{
    // Set configuration options.
});
```

With this call all we are configuring is a mapping a type of consumer (a `TestEventConsumer`) to an event
type (`BasicTestEvent`).
This specifies that when the service bus receives an event of type `BasicTestEvent` it should be passed to an instance
of `TestEventConsumer`.

That's all! Once CustomEventBus has been added as above it registers its self as a BackgroundService, and creates all
the threads and tasks its needs.

# Consumers

Consumers are classes that receive messages from the Event Bus. They need to be injected into the DI container 
(preferably as scoped services) and configured in the Event Bus to map them to the relevant events.

A consumer is a class that handles events incoming on the event bus. See the relevant classes documentation for details on their creation.   
When we have a consumer we wish to use we need to register it with DI. To register the consumer just add it as a Scoped service like so:

```csharp
services.AddScoped<TestEventConsumer>();
```

## Creating a Consumer

Most likely a Consumer will inherit from `BaseSingleConsumer<IMessage>`. You can inherit from IConsumer, but
BaseConsumer has
helper code to map types through to the correct types for consumption. If you would like to create a consumer that
handles multiple
event types with the same code you can use `BaseMultipleConsumer`. This consumer does not validate the types of events
passed to it
it will just pass all events that are mapped to that consumer type.

To receive a message override the OnHandle method, which will pass the message that has just been received from the
message bus.

```csharp
// BaseSingleConsumer
public override Task OnHandle(BasicTestEvent message, MessageMetadata metadata, IMessage? customHeader = null) {
    // Put logic here
}

// BaseMultipleConsumer
public override Task OnHandle(IMessage message, MessageMetadata metadata, IMessage? customHeader = null) {
    // We can use a switch case to write logic for any of the events are are trying to handle.
    switch (message)
    {
        case BasicTestEvent basicTestEvent:
            // Logic goes here
            break;
        case BasicTestEventTwo basicTestEventTwo:
            // Logic goes here
            break;
        default:
            throw new ArgumentOutOfRangeException($"Unknown event type: {message.GetType().ToString()}");
    }
}
```

## Mapping a Consumer

Once the consumer is created we need to tell CustomEventBus to route events to that consumer. The easist way is to map
the consumer using Attributes. The `[EventHandler]` attribute will allow the consumer to automatically consume events
based on the type of the event from its generic arguments.  
For example if we have a handler with the consumer type `public class TestSingleEventConsumer : BaseSingleConsumer<BasicTestEvent>`
Then the attribute will automatically route `BaseTestEvents` to that consumer. If we are using `BaseMultipleConsumer` we
will need to manually provide the types of the event we would like to handle
i.e. `[EventHandler(typeof(BasicTestEvent))]`.
We can use multiple of these attributes to map multiple event types to one consumer.

The other option to to provide a manual mapping of event type to consumer type. This is done in the configuration step
of CustomEventBus, when we call AddEventBus we pass it a list of consumers and their event mappings

```csharp
builder.Services.AddCustomEventBus(configuration =>
{
    configuration.AddConsumerRegistration(typeof(BasicTestEvent),typeof(ExampleConsumer));
});
```

ExampleConsumer will now receive events of type BasicTestEvent.