using System.Text.Json;
using FluentAssertions;
using NumbGoat.CustomEventBus.AwsEventBridge.Models;
using NumbGoat.CustomEventBus.Common;

namespace NumbGoat.CustomEventBus.AwsEventBridge.Tests;

public class EventBridgePatternTests
{
    [Fact]
    public void StandardPattern_Success_00()
    {
        var pattern = new EventBridgeStandardPattern
        {
            Source = new[] { "numbgoat.customeventbus" },
            DetailType = new[]
                { AwsEventBridgeManager.BuildDetailType(MessageTransportType.Topic, "basic-test-event") },
            Detail = new DetailPattern
            {
                // TransportType = new[] { "Topic" },
                TransportName = new[]
                {
                    "basic-test-event"
                }
            }
        };
        var patternJson = pattern.ToJson();
        patternJson.Should().NotBeNull();
        patternJson.Should().NotBeEmpty();
        patternJson.Should()
            .ContainAll("basic-test-event"); // Validate things are named correctly.
        patternJson.Should().NotContainAny("Source", "Detail"); // Make sure kebab case is being used for AWS stuff.
    }
}