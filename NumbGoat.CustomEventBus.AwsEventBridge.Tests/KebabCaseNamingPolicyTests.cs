namespace NumbGoat.CustomEventBus.AwsEventBridge.Tests;

public class KebabCaseNamingPolicyTests
{
    private readonly KebabCaseNamingPolicy _kebabCaseNamingPolicy = new KebabCaseNamingPolicy();

    [Theory]
    [InlineData("FirstName", "first-name")]
    [InlineData("LastName", "last-name")]
    [InlineData("ID", "id")]
    [InlineData("MCU", "mcu")]
    [InlineData("ALOTBSOL", "alotbsol")] // “always look on the bright side of life”
    [InlineData("IPAddress", "ip-address")]
    [InlineData("", "")]
    [InlineData(null, null)]
    [InlineData("Simple", "simple")]
    public void ConvertName_ShouldReturnKebabCase(string input, string expected)
    {
        var result = _kebabCaseNamingPolicy.ConvertName(input);
        Assert.Equal(expected, result);
    }
}