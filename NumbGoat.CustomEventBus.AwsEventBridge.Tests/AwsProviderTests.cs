using System.Diagnostics.CodeAnalysis;
using Amazon.SQS.Model;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Tests.Common;
using NumbGoat.CustomEventBus.Tests.Common.Consumers;
using NumbGoat.CustomEventBus.Tests.Protos;
using Xunit.Abstractions;
using Xunit.DependencyInjection.Logging;

namespace NumbGoat.CustomEventBus.AwsEventBridge.Tests;

public class AwsProviderTests : IAsyncLifetime
{
    private readonly ILogger<AwsProviderTests> _logger;
    private readonly IServiceProvider _serviceProvider;
    private readonly IConfiguration _configuration;
    private readonly ITestOutputHelper _outputHelper;
    private AwsEventBridgeProvider? _provider;
    private IHost? _host;
    private CancellationTokenSource? _cts;
    private static bool IsOnSecondUnitTest { get; set; } = false;

    public AwsProviderTests(ILogger<AwsProviderTests> logger, IServiceProvider serviceProvider,
        IConfiguration configuration, ITestOutputHelper outputHelper)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
        _configuration = configuration;
        _outputHelper = outputHelper;
    }

    public Task InitializeAsync()
    {
        _cts = new CancellationTokenSource();
        HostHelpers.ResetAndRegisterTestEvents();
        return Task.CompletedTask;
    }

    public async Task DisposeAsync()
    {
        _cts?.Cancel();
        if (_provider != null)
        {
            try
            {
                if (IsOnSecondUnitTest)
                {
                    //We need to wait for 60 seconds for the queue to get properly cleared
                    await Task.Delay(TimeSpan.FromSeconds(60));
                }
                
                await _provider.ClearTopic("basic-test-event");
                await _provider.ClearQueue("ceb-basic-test-job");
                
                IsOnSecondUnitTest = true;
            }
            catch (AwsProviderException)
            {
                // Ignore for resetting
            }
        }

        if (_host != null)
        {
            await _host.StopAsync();
        }
    }

    [Fact]
    public async Task Constructor_Success_00()
    {
        (_, AwsEventBridgeProvider provider, _) = await CreateEventBus();
        provider.Should().NotBeNull();
    }

    [Fact]
    public async Task Connect_Success_00()
    {
        (_, AwsEventBridgeProvider provider, _) = await CreateEventBus();
        await provider.Start(ArraySegment<EventDefinition>.Empty, Array.Empty<KeyValuePair<Type, IEnumerable<Type>>>(),
            _cts!.Token);
        SpinWait.SpinUntil(() => _provider is { IsReady: true }, TimeSpan.FromSeconds(30));
        _provider!.IsReady.Should().BeTrue(); // _provider isnt null if CreateEventBus() returns properly
    }

    [Fact]
    public async Task StartEventBus_Success_00()
    {
        (_, AwsEventBridgeProvider provider, _) = await CreateAndStartEventBus();

        provider.IsReady.Should().BeTrue();
    }

    [Fact]
    public async Task ProcessEvent_Success_00()
    {
        (CustomEventBus customEventBus, AwsEventBridgeProvider provider, TestSingleEventConsumer testCounterConsumer) =
            await CreateAndStartEventBus();

        // Create and queue event
        var testMetadata = new BasicTestMetadata
        {
            Version = "v1.0.0"
        };
        var testEvent = new BasicTestEvent
        {
            MessageText = "This is a test :)"
        };
        await customEventBus.Publish(testEvent, testMetadata);
        _logger.LogInformation("Waiting for CustomEventBus to publish event.");
        SpinWait.SpinUntil(() => !customEventBus.HasOutgoingMessages || _cts!.IsCancellationRequested,
            TimeSpan.FromMinutes(2));
        customEventBus.HasOutgoingMessages.Should().BeFalse();
        _logger.LogInformation("Waiting for consumer to receive event.");
        SpinWait.SpinUntil(() => testCounterConsumer.Count >= 1, TimeSpan.FromSeconds(20));
        Thread.Sleep(2000); // To catch the case where an event get received twice, which should fail.
        testCounterConsumer.Count.Should().Be(1);
    }

    #region Helpers

    private void ConfigureTestServices(HostBuilderContext context, IServiceCollection service)
    {
        IConfigurationSection configuredOptions = _configuration.GetSection("AwsEvents");
        service.AddAwsEventProvider(options => { configuredOptions.Bind(options); });
    }

    [MemberNotNullWhen(true, nameof(_provider))]
    private async Task<(CustomEventBus, AwsEventBridgeProvider, TestSingleEventConsumer)> CreateEventBus()
    {
        (IHost host, CustomEventBus customEventBus) =
            await HostHelpers.CreateBasicEventBus(new[] { typeof(AwsEventBridgeProvider) },
                ConfigureTestServices, _outputHelper,
                configure: configuration => { configuration.AutoLoadHandlers = true; });
        _host = host;
        IEventBusProvider? foundProvider = host.Services.GetServices<IEventBusProvider>()
            .FirstOrDefault(p => p.GetType().IsAssignableTo(typeof(AwsEventBridgeProvider)));
        _provider = foundProvider as AwsEventBridgeProvider ??
                    throw new Exception("Could not get AwsEvents provider");
        var consumer = host.Services.GetRequiredService<TestSingleEventConsumer>();
        return (customEventBus, _provider, consumer);
    }


    private async Task<(CustomEventBus, AwsEventBridgeProvider, TestAutoEventConsumer)> CreateAndStartEventBus()
    {
        (IHost host, CustomEventBus customEventBus) =
            await HostHelpers.CreateBasicEventBus(new[] { typeof(AwsEventBridgeProvider) },
                ConfigureTestServices, _outputHelper,
                configure: configuration => { configuration.AutoLoadHandlers = true; });
        _host = host;
        IEventBusProvider? foundProvider = host.Services.GetServices<IEventBusProvider>()
            .FirstOrDefault(p => p.GetType().IsAssignableTo(typeof(AwsEventBridgeProvider)));
        _provider = foundProvider as AwsEventBridgeProvider ??
                    throw new Exception("Could not get AwsEvents provider");
        var consumer = host.Services.GetRequiredService<TestAutoEventConsumer>();
        await HostHelpers.StartEventBusAndWaitForReady(host, _cts?.Token ?? default(CancellationToken));
        return (customEventBus, _provider, consumer);
    }

    #endregion
}