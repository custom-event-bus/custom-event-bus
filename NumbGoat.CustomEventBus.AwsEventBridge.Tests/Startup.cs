﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Xunit.DependencyInjection.Logging;

namespace NumbGoat.CustomEventBus.AwsEventBridge.Tests;

public class Startup
{
    public IHostBuilder CreateHostBuilder()
    {
        if (Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") == null)
        {
            Environment.SetEnvironmentVariable("DOTNET_ENVIRONMENT", "Development");
        }

        return Host.CreateDefaultBuilder().ConfigureLogging(ConfigureLogging).ConfigureServices(ConfigureServices);
    }

    private void ConfigureLogging(HostBuilderContext context, ILoggingBuilder builder)
    {
        builder.AddSerilog();
        builder.AddXunitOutput();
    }

    private void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        services.Configure<AwsEventBridgeOptions>(context.Configuration.GetSection("AwsEventBridge"));
    }
}