using NumbGoat.CustomEventBus.Common.Exceptions;

namespace NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;

public class AwsConfigurationException: EventBusException
{
    public AwsConfigurationException(string message) : base(message)
    {
    }

    public AwsConfigurationException(string message, Exception innerException) : base(message, innerException)
    {
    }
}