using NumbGoat.CustomEventBus.Common.Exceptions;

namespace NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;

public class AwsProviderException: EventBusException
{
    public AwsProviderException(string message) : base(message)
    {
    }

    public AwsProviderException(string message, Exception innerException) : base(message, innerException)
    {
    }
}