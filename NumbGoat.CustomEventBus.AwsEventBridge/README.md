# Custom Event Bus - AWS EventBridge

This provides Queue and Topic capabilities to Custom Event Bus, powered by AWS EventBridge.
To enable this functionality this provider uses SQS and SNS to bind Queues and Topics respectively to consumers.
It also creates the relevant EventBrige rules to route messages to the correct services.

# Queues
Queues in this the EventBridge provider are handle by routing EventBridge events to a SQS queue. The provider then
directly consumes from that queue.

# Topics
Topics are handled by routing events from EventBridge to a SNS topic and then through to a SQS queue that is created
one per consumer. The consumer will have its Machine Name in its queue, unless provided as an option.