using System.Net;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Json;

namespace NumbGoat.CustomEventBus.AwsEventBridge;

public class AwsSqsManager : IHostedService
{
    private readonly ILogger<AwsSqsManager> _logger;
    private readonly AwsCloudManager _cloudManager;
    private readonly AmazonSQSClient _sqsClient;
    private readonly List<Thread> _pollingThreads = new();
    private CancellationTokenSource? _pollingTokenSource = null;
    private readonly AwsEventBridgeOptions _options;

    public AwsSqsManager(ILogger<AwsSqsManager> logger, AwsCloudManager cloudManager,
        IOptions<AwsEventBridgeOptions> options)
    {
        _logger = logger;
        _cloudManager = cloudManager;
        _sqsClient = cloudManager.GetSqsClient();
        _options = options.Value;
    }

    public async Task<IEnumerable<string>> GetAllQueueUrls(string? namePrefix = null,
        CancellationToken? cancellationToken = null)
    {
        CancellationToken ct = cancellationToken ?? CancellationToken.None;

        var allQueueUrls = new List<string>();

        ListQueuesResponse? response = await _sqsClient.ListQueuesAsync(namePrefix, ct);
        allQueueUrls.AddRange(response.QueueUrls);
        while (response.NextToken != null)
        {
            response = await _sqsClient.ListQueuesAsync(new ListQueuesRequest
            {
                QueueNamePrefix = namePrefix,
                NextToken = response.NextToken
            }, ct);
        }

        return allQueueUrls;
    }

    public async Task<IEnumerable<string>> GetAllQueueNames(string? namePrefix = null,
        CancellationToken? cancellationToken = null)
    {
        return (await GetAllQueueUrls(namePrefix, cancellationToken)).Select(ExtractQueueName);
    }

    public async Task CreateQueueIfNotExists(string queueName)
    {
        var existingQueueResponse = await _sqsClient.ListQueuesAsync(queueName);
        if (existingQueueResponse.QueueUrls.Any(qUql => ExtractQueueName(qUql) == queueName))
        {
            return;
        }

        await CreateQueue(queueName);
    }

    public Task<string> CreateQueue(string queueName)
    {
        return CreateQueue(queueName, false);
    }

    private async Task<string> CreateQueue(string queueName, bool secondAttempt)
    {
        CreateQueueResponse? response = null;
        try
        {
            response = await _sqsClient.CreateQueueAsync(new CreateQueueRequest
            {
                QueueName = queueName
            });
        }
        catch (QueueDeletedRecentlyException)
        {
            if (secondAttempt)
            {
                throw;
            }

            _logger.LogWarning("Queue was recently deleted, waiting 60 seconds to recreate");
            Thread.Sleep(TimeSpan.FromSeconds(60));
            return await CreateQueue(queueName, true);
        }

        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException($"Create queue failed: {response.ResponseMetadata}");
        }

        string arn = MakeArnFromQueueName(queueName);
        return arn;
    }

    public async Task<string?> GetQueueUrlByArn(string queueArn)
    {
        if (queueArn == null)
        {
            throw new NullReferenceException(nameof(queueArn));
        }

        var queueUrlResponse = await _sqsClient.GetQueueUrlAsync(new GetQueueUrlRequest
        {
            QueueName = GetQueueNameFromArn(queueArn)
        });
        return queueUrlResponse.QueueUrl;
    }

    public async Task<string?> GetQueueUrlByName(string queueName)
    {
        var queueUrlResponse = await _sqsClient.GetQueueUrlAsync(new GetQueueUrlRequest
        {
            QueueName = queueName
        });
        return queueUrlResponse.QueueUrl;
    }

    public async Task DeleteQueueByName(string queueName)
    {
        var queueUrlResponse = await _sqsClient.GetQueueUrlAsync(queueName);
        if (queueUrlResponse.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException(
                $"Failed to get queue url for deletion: {queueUrlResponse.ResponseMetadata}");
        }

        DeleteQueueResponse? response = await _sqsClient.DeleteQueueAsync(queueUrlResponse.QueueUrl);
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException($"Failed to delete queue \"{queueName}: {response.ResponseMetadata}");
        }
    }

    public Task ListenToQueue(string queueName, EventDefinition eventDefinition,
        Action<string, string, MessageTransportType, Message> handleAction)
    {
        if (_pollingTokenSource == null)
        {
            throw new AwsProviderException("AwsSqsManager has not started. Has it been injected correctly?");
        }

        var listenThread = new Thread(() =>
        {
            try
            {
                PollForMessages(queueName, eventDefinition, handleAction, _pollingTokenSource.Token).Wait();
            }
            catch (Exception e)
            {
                switch (e)
                {
                    case TaskCanceledException:
                        return;
                    case AggregateException aggregateException:
                        if (aggregateException.InnerExceptions.All(ie => ie is TaskCanceledException))
                        {
                            return;
                        }
                        else
                        {
                            throw;
                        }
                    default:
                        throw;
                }
            }
        });
        listenThread.Start();
        _pollingThreads.Add(listenThread);
        return Task.CompletedTask;
    }

    /// <summary>
    /// Removes a message from a queue. This is typically used when the message is handled successfully.
    /// </summary>
    /// <param name="queueName">The name of the queue where the message came from</param>
    /// <param name="message">The message that originated from that queue</param>
    /// <param name="cancellationToken"></param>
    public async Task DeleteMessageFromQueue(string queueName, Message message,
        CancellationToken? cancellationToken = null)
    {
        cancellationToken ??= CancellationToken.None;
        CancellationToken ct = cancellationToken.Value;
        if (string.IsNullOrWhiteSpace(message.ReceiptHandle))
        {
            throw new NullReferenceException(nameof(message.MessageId));
        }

        AmazonSQSClient localSqsClient = _cloudManager.GetSqsClient();
        GetQueueUrlResponse? queueUrlResponse = await localSqsClient.GetQueueUrlAsync(queueName, ct);

        await _sqsClient.DeleteMessageAsync(queueUrlResponse.QueueUrl, message.ReceiptHandle, ct);
    }

    private async Task PollForMessages(string queueName, EventDefinition eventDefinition,
        Action<string, string, MessageTransportType, Message> handleAction,
        CancellationToken cancellationToken)
    {
        var localSqsClient = _cloudManager.GetSqsClient();
        var queueUrlResponse = await localSqsClient.GetQueueUrlAsync(queueName, cancellationToken);
        if (queueUrlResponse.QueueUrl == null)
        {
            throw new AwsProviderException($"Failed to find SQS queue for {queueName}");
        }

        while (!cancellationToken.IsCancellationRequested)
        {
            ReceiveMessageResponse? response = await localSqsClient.ReceiveMessageAsync(new ReceiveMessageRequest
            {
                QueueUrl = queueUrlResponse.QueueUrl,
                VisibilityTimeout = _options.MessageLockSeconds,
                WaitTimeSeconds = 2,
                MaxNumberOfMessages = 1
            }, cancellationToken);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new AwsProviderException($"Failed to get messages from SQS queue. {response.ResponseMetadata}");
            }

            if (!response.Messages.Any())
            {
                continue;
            }

            foreach (Message responseMessage in response.Messages)
            {
                handleAction.Invoke(eventDefinition.TransportName, queueName, eventDefinition.TransportType,
                    responseMessage);
            }
        }
    }

    public async Task ClearQueue(string queueName)
    {
        var queueUrl = await GetQueueUrlByName(queueName);
        _ = await _sqsClient.PurgeQueueAsync(queueUrl);
    }

    private string MakeArnFromQueueName(string queueName)
    {
        var awsAccountId = _cloudManager.GetAwsAccountId();
        var awsRegion = _cloudManager.GetRegionString();
        return $"arn:aws:sqs:{awsRegion}:{awsAccountId}:{queueName}";
    }


    private static string ExtractQueueName(string queueUrl) => queueUrl.Split('/').Last();

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _pollingTokenSource = new CancellationTokenSource();
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _pollingTokenSource?.Cancel();
        foreach (Thread thread in _pollingThreads)
        {
            thread.Join(TimeSpan.FromSeconds(2));
        }

        return Task.CompletedTask;
    }

    /// <summary>
    /// Extracts a topic name from a standard SQS queue arn. Does not validate the input before extracting.
    /// </summary>
    /// <param name="queueArn">The ARN from AWS SDK</param>
    /// <returns>The queue name part from the ARN.</returns>
    private static string GetQueueNameFromArn(string queueArn) => queueArn.Split(':').Last();
}