// ReSharper disable once CheckNamespace
namespace System.Runtime.CompilerServices
{
    /// <summary>
    /// This is here to fix some issues using records with .net standard 2.0
    /// </summary>
    internal static class IsExternalInit {}
}