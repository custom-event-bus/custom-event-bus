using System.Text;
using System.Text.Json;

namespace NumbGoat.CustomEventBus.AwsEventBridge;

public class KebabCaseNamingPolicy : JsonNamingPolicy
{
    public override string ConvertName(string name)
    {
        return string.IsNullOrEmpty(name) ? name : ToKebabCase(name);
    }


    private static string ToKebabCase(string str)
    {
        var result = new StringBuilder();
        
        for (var i = 0; i < str.Length; i++)
        {
            char currentChar = str[i];
            bool isCurrentCharUpper = char.IsUpper(currentChar);

            // Insert a dash if it's an upper character, but not the first character,
            // and either the previous character is not upper or the next character is lower.
            if (i > 0 && isCurrentCharUpper)
            {
                bool isPrevCharLower = !char.IsUpper(str[i - 1]);
                bool isNextCharLower = (i < str.Length - 1) && !char.IsUpper(str[i + 1]);

                if (isPrevCharLower || isNextCharLower)
                {
                    result.Append('-');
                }
            }

            result.Append(currentChar);
        }
        
        return result.ToString().ToLowerInvariant();
    }
}