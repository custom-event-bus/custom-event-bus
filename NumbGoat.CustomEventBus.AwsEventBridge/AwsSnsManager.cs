using System.Net;
using System.Text.Json;
using System.Text.Json.Nodes;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;

namespace NumbGoat.CustomEventBus.AwsEventBridge;

public class AwsSnsManager
{
    private readonly ILogger<AwsSnsManager> _logger;
    private readonly AwsCloudManager _cloudManager;
    private readonly AmazonSimpleNotificationServiceClient _snsClient;

    public static readonly string SqsConsumerIdentifier = "sns-consumer";

    public AwsSnsManager(ILogger<AwsSnsManager> logger, AwsCloudManager cloudManager)
    {
        _logger = logger;
        _cloudManager = cloudManager;
        _snsClient = cloudManager.GetSnsClient();
    }

    public async Task<IEnumerable<string>> GetAllTopicArn(string? topicPrefix = null,
        CancellationToken? cancellationToken = null)
    {
        CancellationToken ct = cancellationToken ?? CancellationToken.None;
        topicPrefix ??= "";

        var topicNames = new List<string>();
        ListTopicsResponse? response = await _snsClient.ListTopicsAsync(ct);
        topicNames.AddRange(response.Topics.Where(t => GetTopicNameFromArn(t.TopicArn).StartsWith(topicPrefix))
            .Select(t => t.TopicArn));

        while (response.NextToken != null)
        {
            response = await _snsClient.ListTopicsAsync(new ListTopicsRequest
            {
                NextToken = response.NextToken
            }, ct);
            topicNames.AddRange(response.Topics.Where(t => GetTopicNameFromArn(t.TopicArn).StartsWith(topicPrefix))
                .Select(t => t.TopicArn));
        }

        return topicNames;
    }

    public async Task<IEnumerable<string>> GetAllTopicNames(string? topicPrefix = null,
        CancellationToken? cancellationToken = null)
    {
        return (await GetAllTopicArn(topicPrefix, cancellationToken)).Select(GetTopicNameFromArn);
    }

    /// <summary>
    /// Creates a new SNS topic, and returns its ARN
    /// </summary>
    /// <param name="topicName"></param>
    /// <returns></returns>
    /// <exception cref="AwsProviderException"></exception>
    public async Task<string> CreateTopic(string topicName)
    {
        var response = await _snsClient.CreateTopicAsync(new CreateTopicRequest
        {
            Name = topicName
        });
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException($"Failed to create SNS topic: {response.ResponseMetadata}");
        }
        
        return response.TopicArn;
    }

    /// <summary>
    /// Validates that event bridge has permissions to publish to SNS topic.
    /// </summary>
    /// <param name="topicName">The topic to set permissions for.</param>
    public async Task ValidateTopicPermissions(string topicName)
    {
        Topic? topic = await _snsClient.FindTopicAsync(topicName);
        if (topic == null)
        {
            throw new AwsProviderException($"Failed to find topic {topicName}");
        }

        GetTopicAttributesResponse? existingAttributes = await _snsClient.GetTopicAttributesAsync(topic.TopicArn);
        if (existingAttributes.Attributes["Policy"].Contains(@"""Sid"":""AllowEventBridgeToPublish"""))
        {
            return;
        }
        
        var currentPolicyJson = existingAttributes.Attributes["Policy"];
        using var doc = JsonDocument.Parse(currentPolicyJson);
        var root = doc.RootElement;

        // Clone the existing policy to a modifiable JsonObject
        var policyObject = JsonNode.Parse(root.GetRawText()) ?? throw new NullReferenceException();
        
        // Create the new policy statement
        var newPolicyStatement = new JsonObject
        {
            ["Sid"] = "AllowEventBridgeToPublish",
            ["Effect"] = "Allow",
            ["Principal"] = new JsonObject { ["Service"] = "events.amazonaws.com" },
            ["Action"] = "sns:Publish",
            ["Resource"] = topic.TopicArn
        };
        
        // Add the new statement to the existing policy's statements array
        if (policyObject["Statement"] is JsonArray statements)
        {
            statements.Add(newPolicyStatement);
        }
        else
        {
            // Handle case where there's no existing statement array
            var newStatements = new JsonArray { newPolicyStatement };
            policyObject["Statement"] = newStatements;
        }
        var policyRequest = new SetTopicAttributesRequest
        {
            TopicArn = topic.TopicArn,
            AttributeName = "Policy",
            AttributeValue = policyObject.ToString()
        };
        SetTopicAttributesResponse? updatePolicyResponse = await _snsClient.SetTopicAttributesAsync(policyRequest);
        if (updatePolicyResponse.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException($"Failed to create SNS topic: {updatePolicyResponse.ResponseMetadata}");
        }

    }

    public async Task DeleteTopic(string topicName, CancellationToken? cancellationToken = null)
    {
        var ct = cancellationToken ?? CancellationToken.None;
        var topicArn = await GetTopicArn(topicName);
        if (topicArn == null)
        {
            throw new AwsProviderException($"Failed to find queue {topicName} for deletion");
        }

        DeleteTopicResponse? response = await _snsClient.DeleteTopicAsync(new DeleteTopicRequest
        {
            TopicArn = topicArn
        }, ct);
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException($"Failed to delete topic: {response.ResponseMetadata}");
        }
    }

    public async Task SubscribeSqsConsumerToTopic(string topicName, string queueArn, string queueUrl)
    {
        var topicArn = await GetTopicArn(topicName);

        // Remove existing subs
        ListSubscriptionsResponse? existingSubscriptions = null;
        bool subscriptionExists = false;
        do
        {
            existingSubscriptions = await _snsClient.ListSubscriptionsAsync(new ListSubscriptionsRequest
            {
                NextToken = existingSubscriptions?.NextToken ?? null
            });
            foreach (Subscription existingSub in existingSubscriptions.Subscriptions.Where(sub =>
                         sub.Endpoint == queueArn))
            {
                if (existingSub.Endpoint == queueUrl && existingSub.TopicArn == topicArn)
                {
                    // Dont remove the one we are adding.
                    subscriptionExists = true;
                    continue;
                }

                await _snsClient.UnsubscribeAsync(existingSub.SubscriptionArn);
            }
        } while (existingSubscriptions.NextToken != null);

        if (subscriptionExists)
        {
            return;
        }

        // Subscribe the topic to the queue
        var subscribeRequest = new SubscribeRequest
        {
            TopicArn = topicArn,
            Protocol = "sqs",
            Endpoint = queueArn,
            Attributes = new Dictionary<string, string>
            {
                { "RawMessageDelivery", "true" } // Enabling raw message delivery
            }
        };
        SubscribeResponse? response = await _snsClient.SubscribeAsync(subscribeRequest);
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException(
                $"Failed to subscribe the SNS topic to the consumer queue {response.ResponseMetadata}");
        }
    }

    /// <summary>
    /// Unsubscribes a SQS queue from the topic
    /// </summary>
    /// <param name="topicName">The name of the topic to unsubscribe</param>
    /// <param name="queueUrl">The url of the queue to unsubscribe</param>
    /// <exception cref="AwsProviderException">Thrown if the queue or topic could not be found.</exception>
    public async Task UnsubscribeSqsConsumerFromTopic(string topicName, string queueUrl)
    {
        AmazonSQSClient sqsClient = _cloudManager.GetSqsClient();
        var sqsAttributes = await sqsClient.GetQueueAttributesAsync(new GetQueueAttributesRequest
        {
            QueueUrl = queueUrl
        });
        var sqsArn = sqsAttributes.QueueARN;

        ListSubscriptionsByTopicResponse? existingSubscription =
            await _snsClient.ListSubscriptionsByTopicAsync(topicName);
        Subscription? sqsSubscription =
            existingSubscription.Subscriptions.FirstOrDefault(s => s.Protocol == "sqs" && s.Endpoint == sqsArn);
        while (sqsSubscription == null && existingSubscription.NextToken != null)
        {
            existingSubscription =
                await _snsClient.ListSubscriptionsByTopicAsync(topicName, existingSubscription.NextToken);
            sqsSubscription =
                existingSubscription.Subscriptions.FirstOrDefault(s => s.Protocol == "sqs" && s.Endpoint == sqsArn);
        }

        if (existingSubscription == null)
        {
            throw new AwsProviderException("Could not find a subscription to unsubscribe from");
        }

        UnsubscribeResponse? unsubscribeResponse = await _snsClient.UnsubscribeAsync(new UnsubscribeRequest
        {
            SubscriptionArn = sqsSubscription?.SubscriptionArn
        });
        if (unsubscribeResponse.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException($"{unsubscribeResponse.ResponseMetadata}");
        }
    }

    public Task<string> GetTopicArn(string topicName)
    {
        string accountId = _cloudManager.GetAwsAccountId();
        string regionName = _cloudManager.GetRegionString();
        return Task.FromResult($"arn:aws:sns:{regionName}:{accountId}:{topicName}");
    }

    /// <summary>
    /// Extracts a topic name from a standard SNS topic arn. Does not validate the input before extracting.
    /// </summary>
    /// <param name="topicArn">The ARN from AWS SDK</param>
    /// <returns>The topic name part from the ARN.</returns>
    private static string GetTopicNameFromArn(string topicArn) => topicArn.Split(':').Last();

    public static string SqsConsumerQueueInfix(string topicName) => $"{SqsConsumerIdentifier}-{topicName}";
}