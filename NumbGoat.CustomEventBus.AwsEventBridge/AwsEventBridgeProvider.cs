using System.Collections.Concurrent;
using System.Text.Json.Nodes;
using Amazon;
using Amazon.SQS.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;
using NumbGoat.CustomEventBus.AwsEventBridge.Models;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Json;

namespace NumbGoat.CustomEventBus.AwsEventBridge;

public class AwsEventBridgeProvider : BaseJsonEventBusProvider
{
    private readonly ILogger<AwsEventBridgeProvider> _logger;
    private readonly AwsSqsManager _sqsManager;
    private readonly AwsSnsManager _snsManager;
    private readonly AwsEventBridgeManager _eventBridgeManager;
    private readonly ConcurrentQueue<JsonMessage> _outgoingMessages = new();
    private readonly AwsEventBridgeOptions _options;
    private readonly ConcurrentDictionary<string, InFlightMessageContainer> _inFlightMessages = new();
    private Thread? _runThread;
    private bool _transportsCreated = false;
    private bool _messageInFlight = false;

    public AwsEventBridgeProvider(ILogger<AwsEventBridgeProvider> logger,
        IOptions<AwsEventBridgeOptions> options, AwsSqsManager sqsManager, AwsSnsManager snsManager,
        AwsEventBridgeManager eventBridgeManager) : base(logger)
    {
        _logger = logger;
        _sqsManager = sqsManager;
        _snsManager = snsManager;
        _eventBridgeManager = eventBridgeManager;
        _options = options.Value;
    }

    public override bool IsReady => _transportsCreated && _runThread != null;
    public override bool HasOutgoingMessages => !_outgoingMessages.IsEmpty || _messageInFlight;
    public override bool SupportsQueues() => true;

    public override bool SupportsTopics() => true;

    protected override async Task OnMessageComplete(IIncomingMessage completedMessage)
    {
        bool gotMessageSuccessfully =
            _inFlightMessages.TryGetValue(completedMessage.MessageId, out InFlightMessageContainer? inFlightMessage);
        if (!gotMessageSuccessfully || inFlightMessage == null)
        {
            _logger.LogError("Failed to retrieve inflight message for {MessageId}, was it handled elsewhere?",
                completedMessage.MessageId);
            return;
        }

        await _sqsManager.DeleteMessageFromQueue(inFlightMessage.ConsumerQueue, inFlightMessage.Message);
    }

    protected override Task OnMessageFailure(IIncomingMessage failedMessage)
    {
        // For SQS we cant mark the message as failed like in other providers, we just need to wait for it to timeout.
        return Task.CompletedTask;
    }

    protected override Task PublishToQueue(JsonMessage message)
    {
        _outgoingMessages.Enqueue(message);
        return Task.CompletedTask;
    }

    protected override Task PublishToTopic(JsonMessage message)
    {
        _outgoingMessages.Enqueue(message);
        return Task.CompletedTask;
    }

    public async Task ClearQueue(string queueName)
    {
        try
        {
            await _sqsManager.ClearQueue(queueName);
        }
        catch (QueueDoesNotExistException)
        {
            // Ignore
        }
    }

    public async Task RemoveSubscription(string topicName)
    {
        // Unsubscribe SQS from SNS
        EventDefinition? eventDefinition = EventRegistry.GetDefinition(topicName);
        if (eventDefinition == null)
        {
            throw new NullReferenceException($"Could not find a event definition for {topicName}");
        }

        string consumerQueueName = GetSqsSnsConsumerQueueName(
            _options.FormattedNamingPrefix + eventDefinition.TransportName, _options.ConsumerName);
        string? queueUrl;
        try
        {
            queueUrl = await _sqsManager.GetQueueUrlByName(consumerQueueName);
        }
        catch (QueueDoesNotExistException)
        {
            throw new AwsProviderException($"Could not find consumer queue for topic {consumerQueueName}");
        }

        if (queueUrl == null)
        {
            throw new AwsProviderException($"Could not find queue {consumerQueueName}");
        }

        await _snsManager.UnsubscribeSqsConsumerFromTopic(topicName, consumerQueueName);

        await _sqsManager.DeleteQueueByName(consumerQueueName);
    }

    public async Task ClearTopic(string topicName)
    {
        string consumerQueueName = GetSqsSnsConsumerQueueName(
            _options.FormattedNamingPrefix + topicName, _options.ConsumerName);
        try
        {
            await _sqsManager.ClearQueue(consumerQueueName);
        }
        catch (QueueDoesNotExistException)
        {
            // Ignore
        }
    }

    public override async Task Start(IEnumerable<EventDefinition> eventTypes,
        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> registeredConsumers, CancellationToken stoppingToken)
    {
        IEnumerable<EventDefinition> eventDefinitions = eventTypes.ToList();
        List<KeyValuePair<Type, IEnumerable<Type>>>? consumerEventsList = registeredConsumers.ToList();
        await base.Start(eventDefinitions, consumerEventsList, stoppingToken);

        // Verify options
        _options.Validate();
        RegionEndpoint? awsRegion = RegionEndpoint.GetBySystemName(_options.AwsRegion);
        if (awsRegion == null)
        {
            throw new AwsConfigurationException("Region is not a valid AWS region");
        }

        _runThread = new Thread(ThreadStart);
        _runThread.Start(); // Start the thread and return back to the service for normal activity
        return;

        async void ThreadStart()
        {
            // Create a SQS queue for each queue, and a SNS topic for each topic
            string transportPrefix = _options.NamingPrefix + "-";
            if (_options.ShouldCreateQueues)
            {
                _logger.LogDebug("Creating queues");
                List<string> queuesNames = eventDefinitions.Where(e => e.TransportType == MessageTransportType.Queue)
                    .Select(d => _options.FormattedNamingPrefix + d.TransportName).ToList();
                List<string> allQueueNames = (await _sqsManager.GetAllQueueNames(transportPrefix)).ToList();

                foreach (string queueName in queuesNames.Where(queueName => !allQueueNames.Contains(queueName)))
                {
                    _ = await _eventBridgeManager.PutEventBridgeRule(queueName, MessageTransportType.Queue);
                    string queueUrl = await _sqsManager.CreateQueue(queueName);
                    await _eventBridgeManager.BindRuleToQueue(queueName, queueUrl);
                }

                if (_options.CleanUpOldTransports)
                {
                    // TODO Clean up old EventBridge rules.
                    // TODO Clean up old consumer queues
                    _logger.LogDebug("Cleaning up old queues");

                    // List all the old queues that dont have name overlaps with ours, and that arnt SqsConsumerQueues.
                    IEnumerable<string> oldQueues = allQueueNames.Where(name =>
                        !queuesNames.Contains(name)).Where(name => !name.Contains(AwsSnsManager.SqsConsumerIdentifier));
                    foreach (string oldQueue in oldQueues)
                    {
                        _logger.LogDebug("Removing old queue {QueueName}", oldQueue);
                        await _sqsManager.DeleteQueueByName(oldQueue);
                    }
                }
            }

            if (_options.ShouldCreateTopics)
            {
                _logger.LogDebug("Creating topics");
                List<string> topicNames = eventDefinitions.Where(e => e.TransportType == MessageTransportType.Topic)
                    .Select(d => _options.FormattedNamingPrefix + d.TransportName).ToList();
                List<string> allTopicNames = (await _snsManager.GetAllTopicNames(transportPrefix)).ToList();

                foreach (string topicName in topicNames)
                {
                    string arn;
                    if (!allTopicNames.Contains(topicName))
                    {
                        arn = await _snsManager.CreateTopic(topicName);
                    }
                    else
                    {
                        arn = await _snsManager.GetTopicArn(topicName) ?? throw new NullReferenceException();
                    }

                    await _snsManager.ValidateTopicPermissions(topicName);

                    _ = await _eventBridgeManager.PutEventBridgeRule(topicName, MessageTransportType.Topic);
                    await _eventBridgeManager.BindRuleToTopic(topicName, arn);
                }

                if (_options.CleanUpOldTransports)
                {
                    _logger.LogDebug("Cleaning up old topics");
                    IEnumerable<string> oldTopics = allTopicNames.Where(name => !topicNames.Contains(name));
                    foreach (string oldTopic in oldTopics)
                    {
                        _logger.LogDebug("Removing old topic {TopicName}", oldTopic);
                        await _snsManager.DeleteTopic(oldTopic);
                    }
                }
            }

            // Bind EventBridge transports Topics/Queues
            await _sqsManager.StartAsync(stoppingToken);

            // Bind queues that have consumers
            foreach ((Type? eventType, IEnumerable<Type>? consumerTypes) in consumerEventsList.Select(pair =>
                         (pair.Key, pair.Value)))
            {
                EventDefinition? eventDefinition = EventRegistry.GetDefinition(eventType);
                if (eventDefinition == null)
                {
                    throw new AwsProviderException($"Failed to find event definition for {eventType}");
                }

                switch (eventDefinition.TransportType)
                {
                    case MessageTransportType.Queue:
                        // Consume from SQS. Assumes queue exists.
                        await _sqsManager.ListenToQueue(_options.FormattedNamingPrefix + eventDefinition.TransportName,
                            eventDefinition, HandleMessages);
                        break;
                    case MessageTransportType.Topic:
                        // Create SQS consumer queue, bind to SNS, consumer from the SQS queue
                        var consumerQueueName = GetSqsSnsConsumerQueueName(
                            _options.FormattedNamingPrefix + eventDefinition.TransportName, _options.ConsumerName);
                        var queueArn = await _sqsManager.CreateQueue(consumerQueueName);
                        var queueUrl = await _sqsManager.GetQueueUrlByArn(queueArn) ??
                                       throw new NullReferenceException();
                        await _snsManager.SubscribeSqsConsumerToTopic(
                            _options.FormattedNamingPrefix + eventDefinition.TransportName, queueArn, queueUrl);
                        await _sqsManager.ListenToQueue(consumerQueueName, eventDefinition, HandleMessages);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            _transportsCreated = true;
            // Run our sending loop
            await Run(stoppingToken);
        }
    }

    private async void HandleMessages(string transportName, string consumerQueue, MessageTransportType transportType,
        Message message)
    {
        _inFlightMessages.TryAdd(message.MessageId,
            new InFlightMessageContainer(transportName, consumerQueue, message));
        string messageDetail = UnwrapEventBridgeMessage(message);

        await InvokeSubscriber(new IncomingJsonMessage(messageDetail, message.MessageId,
            new MessageMetadata(transportName, transportType)));
    }

    /// <summary>
    /// Unwraps the excess json that EventBridge adds to a Message and just returns the JSON that is needed for CEB
    /// </summary>
    public static string UnwrapEventBridgeMessage(Message message)
    {
        // Unwind the EventBridge wrapper
        var messageObject = JsonNode.Parse(message.Body) ?? throw new NullReferenceException();
        var messageDetail = messageObject["detail"] ?? throw new NullReferenceException();
        return messageDetail.ToJsonString();
    }

    private async Task Run(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            // Outgoing event send loop

            // Sleep until there is something to do.
            SpinWait.SpinUntil(() => HasOutgoingMessages || stoppingToken.IsCancellationRequested);

            try
            {
                bool dequeueSuccess = _outgoingMessages.TryDequeue(out JsonMessage? message);
                if (!dequeueSuccess)
                {
                    _logger.LogTrace("Failed to dequeue message properly");
                    continue;
                }

                _messageInFlight = true;

                if (message == null)
                {
                    throw new NullReferenceException("Dequeued message was null");
                }

                _logger.LogTrace("Sending message to {TransportName}", message.Metadata.TransportName);
                await _eventBridgeManager.PublishMessage(message,
                    _options.FormattedNamingPrefix + message.Metadata.TransportName);
            }
            finally
            {
                _messageInFlight = false;
            }
        }
    }

    public static string GetSqsSnsConsumerQueueName(string topicName,
        string machineName) => $"{topicName}-{AwsSnsManager.SqsConsumerIdentifier}-{machineName}";
}