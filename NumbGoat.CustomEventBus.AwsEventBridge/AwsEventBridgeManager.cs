using System.Net;
using System.Text.RegularExpressions;
using Amazon.EventBridge;
using Amazon.EventBridge.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;
using NumbGoat.CustomEventBus.AwsEventBridge.Models;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Json;

namespace NumbGoat.CustomEventBus.AwsEventBridge;

public class AwsEventBridgeManager
{
    private readonly ILogger<AwsEventBridgeManager> _logger;
    private readonly AwsEventBridgeOptions _options;
    private readonly AmazonEventBridgeClient _client;
    private readonly string? _busName;
    private readonly string _sourceName;

    public AwsEventBridgeManager(ILogger<AwsEventBridgeManager> logger, IOptions<AwsEventBridgeOptions> options,
        AwsCloudManager awsCloudManager)
    {
        _logger = logger;
        _options = options.Value;
        _client = awsCloudManager.GetEventBridgeClient();
        _options.Validate();
        _busName = _options.UseDefaultBus ? null : _options.BusName;
        _sourceName = _options.EventSourceName;
    }

    /// <summary>
    /// Creates (or replaces if rule exists) 
    /// </summary>
    /// <param name="transportName">The name of the queue or topic to create rule for</param>
    /// <param name="eventType">Transport type that matches the <see cref="transportName"/></param>
    /// <returns>The name of the EventBridge rule that was created.</returns>
    /// <exception cref="AwsProviderException">There was issues creating, or updating the rule</exception>
    public async Task<string> PutEventBridgeRule(string transportName, MessageTransportType eventType)
    {
        var eventTypeAsString = eventType.ToString();
        var eventPattern = new EventBridgeStandardPattern
        {
            Source = [_options.EventSourceName],
            DetailType = [BuildDetailType(eventType, transportName)],
            // DetailType = ["*"],
            // Detail = new DetailPattern
            // {
            //     // TransportName = [transportName],
            //     // TransportType = [eventTypeAsString]
            // }
        };
        string eventPatternJson = eventPattern.ToJson();
        string ruleName = BuildRuleName(_options.RulePrefix, eventTypeAsString, transportName);

        // Check for existing rule
        ListRulesResponse? checkResponse = await _client.ListRulesAsync(new ListRulesRequest
        {
            EventBusName = _busName,
            NamePrefix = ruleName
        });
        Rule? existingRule = checkResponse.Rules.FirstOrDefault();
        if (existingRule != null)
        {
            string eventPatternNoWhitespace = Regex.Replace(eventPatternJson, @"\s", "");
            string existingEventNoWhitespace = Regex.Replace(existingRule.EventPattern, @"\s", "");
            if (!eventPatternNoWhitespace.Equals(existingEventNoWhitespace, StringComparison.OrdinalIgnoreCase))
            {
                // Get rule targets
                var targets = await _client.ListTargetsByRuleAsync(new ListTargetsByRuleRequest
                {
                    EventBusName = _busName,
                    Rule = existingRule.Name
                });
                if (targets.Targets.Count > 0)
                {
                    if (targets.NextToken != null)
                    {
                        throw new AwsProviderException("More targets exist than support to remove");
                    }

                    var removeTargetRequest = new RemoveTargetsRequest
                    {
                        EventBusName = _busName,
                        Rule = existingRule.Name,
                        Ids = targets.Targets.Select(t => t.Id).ToList(),
                        Force = true
                    };
                    RemoveTargetsResponse? removeTargetResponse = await _client.RemoveTargetsAsync(removeTargetRequest);
                    if (removeTargetResponse.FailedEntries.Any())
                    {
                        throw new AwsProviderException($"Failed to remove rule targets");
                    }
                }

                // Drop the rule and replace below.
                DeleteRuleResponse? deleteResponse = await _client.DeleteRuleAsync(new DeleteRuleRequest
                {
                    EventBusName = _busName,
                    Name = ruleName,
                    Force = true
                });
                if (deleteResponse.HttpStatusCode != HttpStatusCode.OK)
                {
                    throw new AwsProviderException(
                        $"Failed to remove old stale event bridge rule {ruleName}: {deleteResponse.ResponseMetadata}");
                }
            }
            else
            {
                // Rule matches our pattern
                return ruleName;
            }
        }

        // Put rule
        PutRuleResponse? response = await _client.PutRuleAsync(new PutRuleRequest
        {
            Name = ruleName,
            Description =
                $"Custom Event Bus. Routes {transportName} events to the {eventTypeAsString.ToLower()} handlers.",
            State = RuleState.ENABLED,
            EventBusName = _busName,
            EventPattern = eventPatternJson
        });

        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AwsProviderException($"Failed to update Event Bridge rule {response.ResponseMetadata}");
        }

        return ruleName;
    }

    /// <summary>
    /// Binds a EventBridge rule (as defined by the providers standard) to the sqsArn (returned from SQS manager)
    /// </summary>
    /// <param name="transportName">The queue name</param>
    /// <param name="sqsArn">The ARN (<b>NOT</b> url) of the SQS queue to bind</param>
    /// <returns></returns>
    public Task BindRuleToQueue(string transportName, string sqsArn)
    {
        // Check for existing rule
        string ruleName = BuildRuleName(_options.RulePrefix, MessageTransportType.Queue.ToString(), transportName);
        string targetId = BuildRuleTargetId(_options.RulePrefix, MessageTransportType.Queue.ToString(), transportName);

        return BindRuleTo(ruleName, targetId, sqsArn);
    }

    /// <summary>
    /// Binds a EventBridge rule (as defined by the providers standard) to the snsArn (returned from SNS manager)
    /// </summary>
    /// <param name="transportName">The topic name</param>
    /// <param name="snsArn">The ARN of the SNS queue to bind</param>
    /// <returns></returns>
    public Task BindRuleToTopic(string transportName, string snsArn)
    {
        // Check for existing rule
        string ruleName = BuildRuleName(_options.RulePrefix, MessageTransportType.Topic.ToString(), transportName);
        string targetId = BuildRuleTargetId(_options.RulePrefix, MessageTransportType.Topic.ToString(), transportName);

        return BindRuleTo(ruleName, targetId, snsArn);
    }

    private async Task BindRuleTo(string ruleName, string targetId, string targetArn)
    {
        var existingTargets = new List<Target>();
        try
        {
            var rule = await _client.DescribeRuleAsync(new DescribeRuleRequest
            {
                EventBusName = _busName,
                Name = ruleName
            });
            if (rule != null)
            {
                var ruleTargets = await _client.ListTargetsByRuleAsync(new ListTargetsByRuleRequest
                {
                    EventBusName = _busName,
                    Rule = ruleName
                });
                existingTargets = ruleTargets.Targets;
                Target? target = existingTargets.FirstOrDefault(t => t.Id == targetId);
                if (target != null)
                {
                    if (target.Arn == targetArn)
                    {
                        // Current rule is valid.
                        return;
                    }

                    // Remove incorrect target
                    existingTargets.Remove(target);
                }
            }
        }
        catch (ResourceNotFoundException)
        {
            // No rule found, continue
        }


        existingTargets.Add(new Target
        {
            Id = targetId,
            Arn = targetArn,
        });

        var response = await _client.PutTargetsAsync(new PutTargetsRequest
        {
            EventBusName = _busName,
            Rule = ruleName,
            Targets = existingTargets
        });
        if (response.FailedEntryCount > 1)
        {
            throw new AwsProviderException(
                $"Failed to bind rule to SQS {response.FailedEntries.Where(f => f.ErrorMessage != null).ToList()}");
        }
    }

    public async Task PublishMessage(JsonMessage message, string eventBridgeTransportName)
    {
        var request = new PutEventsRequest
        {
            Entries =
            [
                new PutEventsRequestEntry
                {
                    EventBusName = _busName,
                    Source = _sourceName,
                    Time = DateTime.UtcNow,
                    Detail = message.MessageJson,
                    // We have to append the event bridge specific naming prefix to the detail type manually
                    DetailType = BuildDetailType(message.Metadata.TransportType,
                        eventBridgeTransportName)
                }
            ]
        };
        PutEventsResponse? response = await _client.PutEventsAsync(request);
        if (response.FailedEntryCount > 1)
        {
            throw new AwsProviderException(
                $"Failed to publish event {response.Entries.Where(e => e.ErrorMessage != null).Select(e => e.ErrorMessage)}");
        }
    }

    public static string BuildDetailType(MessageTransportType transportType, string transportName) =>
        $"{transportType.ToString().ToLower()}:{transportName}";

    public static string BuildRuleName(string rulePrefix, string eventTypeString, string transportName) =>
        $"{rulePrefix}-{eventTypeString}-{transportName}-consumers".ToLower();

    public static string BuildRuleTargetId(string rulePrefix, string eventTypeString, string transportName) =>
        $"{rulePrefix}-{eventTypeString}-{transportName}-consumers".ToLower();
}