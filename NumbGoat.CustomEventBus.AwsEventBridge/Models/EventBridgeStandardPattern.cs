using System.Text.Json;
using System.Text.Json.Serialization;

namespace NumbGoat.CustomEventBus.AwsEventBridge.Models;

public class EventBridgeStandardPattern
{
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public DetailPattern? Detail { get; set; } = null;
    public string[] Source { get; set; } = Array.Empty<string>();
    public string[] DetailType { get; set; } = Array.Empty<string>();

    public string ToJson()
    {
        return JsonSerializer.Serialize(this, new JsonSerializerOptions
        {
            PropertyNamingPolicy = new KebabCaseNamingPolicy(),
        });
    }
}

public class DetailPattern
{
    // [JsonPropertyName("transportType")]
    //
    // public string[] TransportType { get; set; } = Array.Empty<string>();

    [JsonPropertyName("transportName")]
    public string[] TransportName { get; set; } = Array.Empty<string>();
}