using Amazon.SQS.Model;

namespace NumbGoat.CustomEventBus.AwsEventBridge.Models;

/// <summary>
/// Represents a message in flight
/// </summary>
/// <param name="TransportName">The name CEB transport that this message originates from.</param>
/// <param name="ConsumerQueue">The name of the SQS consumer queue that this message is from.</param>
/// <param name="Message">The message from SQS</param>
internal record InFlightMessageContainer(string TransportName, string ConsumerQueue, Message Message);