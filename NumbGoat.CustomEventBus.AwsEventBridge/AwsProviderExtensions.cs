using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using NumbGoat.CustomEventBus.Common;

namespace NumbGoat.CustomEventBus.AwsEventBridge;

public static class AwsProviderExtensions
{
    public static void AddAwsEventProvider(this IHostBuilder hostBuilder)
    {
    }

    public static void AddAwsEventProvider(this IServiceCollection serviceCollection,
        Action<AwsEventBridgeOptions>? configure = null)
    {
        configure ??= options => { };
        serviceCollection.Configure(configure);

        serviceCollection.TryAddSingleton<AwsCloudManager>();
        serviceCollection.TryAddSingleton<AwsSqsManager>();
        serviceCollection.AddHostedService(provider => provider.GetRequiredService<AwsSqsManager>());
        serviceCollection.TryAddSingleton<AwsSnsManager>();
        serviceCollection.TryAddSingleton<AwsEventBridgeManager>();
        serviceCollection.TryAddSingleton<IEventBusProvider,AwsEventBridgeProvider>();
    }
}