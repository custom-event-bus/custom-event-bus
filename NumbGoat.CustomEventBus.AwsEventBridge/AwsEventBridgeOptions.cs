using Amazon;
using Amazon.Internal;
using NumbGoat.CustomEventBus.AwsEventBridge.Exceptions;
using System.Linq;

namespace NumbGoat.CustomEventBus.AwsEventBridge
{
    /// <summary>
    /// Provides a set of options for configuring AWS EventBridge functionality.
    /// </summary>
    public record AwsEventBridgeOptions
    {
        private static readonly char[] InvalidNamingChars = { ' ', ',', ';' };

        /// <summary>
        /// Gets or sets the AWS Access Key. If not provided, a profile from the machine will be used.
        /// </summary>
        public string? AwsAccessKey { get; set; }

        /// <summary>
        /// Gets or sets the AWS Secret Access Key. If not provided, a profile from the machine will be used.
        /// </summary>
        public string? AwsSecretAccessKey { get; set; }

        /// <summary>
        /// Gets or sets the AWS region. This is a required field and must be a valid AWS region.
        /// </summary>
        public string? AwsRegion { get; set; }

        /// <summary>
        /// Gets or sets the AWS profile to be used when AWS credentials are not provided.
        /// </summary>
        public string? AwsProfile { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use the default AWS EventBridge bus. If set to false, BusName must be provided.
        /// </summary>
        public bool UseDefaultBus { get; set; } = false;

        /// <summary>
        /// Gets or sets the custom AWS EventBridge bus name. This must be set if UseDefaultBus is false.
        /// </summary>
        public string? BusName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to create AWS SQS queues.
        /// </summary>
        public bool ShouldCreateQueues { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether to create AWS SNS topics.
        /// </summary>
        public bool ShouldCreateTopics { get; set; } = true;

        /// <summary>
        /// Gets or sets the prefix for AWS EventBridge rule names. Must not contain spaces, commas, or semicolons.
        /// </summary>
        public string RulePrefix { get; set; } = "ceb";

        /// <summary>
        /// Gets or sets the naming prefix for AWS resources. Must not contain spaces, commas, or semicolons.
        /// </summary>
        public string NamingPrefix { get; set; } = "ceb";

        /// <summary>
        /// Gets the naming prefix in the format for appending.
        /// </summary>
        public string FormattedNamingPrefix => $"{(string.IsNullOrWhiteSpace(NamingPrefix) ? "" : $"{NamingPrefix}-")}";

        /// <summary>
        /// Gets or sets the source name for events. Must be a valid non-empty string.
        /// </summary>
        public string EventSourceName { get; set; } = "numbgoat.ceb";

        /// <summary>
        /// Gets or sets a value indicating whether to clean up old transports. Does not clean up consumer queues.
        /// </summary>
        public bool CleanUpOldTransports { get; set; } = true;

        /// <summary>
        /// Gets or sets the duration, in seconds, for which a client will hold a message before it becomes visible again to other consumers.
        /// </summary>
        public int MessageLockSeconds { get; set; } = 120;

        /// <summary>
        /// The name that this consumer is referred to by.
        /// </summary>
        public string ConsumerName { get; set; } = Environment.MachineName;

        /// <summary>
        /// Validates the current options configuration.
        /// </summary>
        /// <exception cref="AwsConfigurationException">
        /// Thrown when the options are not currently a valid set:
        /// <list type="bullet">
        /// <item>
        /// <description>AWS Region is a required field and must be a valid AWS region.</description>
        /// </item>
        /// <item>
        /// <description>If UseDefaultBus is set to false, BusName must be provided.</description>
        /// </item>
        /// <item>
        /// <description>RulePrefix and NamingPrefix must not contain invalid characters such as spaces, commas, or semicolons.</description>
        /// </item>
        /// <item>
        /// <description>EventSourceName must be a valid non-empty string.</description>
        /// </item>
        /// </list>
        /// </exception>
        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(AwsRegion))
            {
                throw new AwsConfigurationException("Region is null or empty");
            }

            RegionEndpoint? awsRegion = RegionEndpoint.GetBySystemName(AwsRegion);
            if (awsRegion == null)
            {
                throw new AwsConfigurationException("Region is not a valid AWS region");
            }

            if (!UseDefaultBus && string.IsNullOrWhiteSpace(BusName))
            {
                throw new AwsConfigurationException("BusName must be set if UseDefaultBus is false");
            }

            if (InvalidNamingChars.Any(c => RulePrefix.Contains(c) || NamingPrefix.Contains(c)))
            {
                throw new AwsConfigurationException(
                    $"Naming prefixes must not contain the following chars {InvalidNamingChars}");
            }

            if (string.IsNullOrWhiteSpace(EventSourceName))
            {
                throw new AwsConfigurationException($"{nameof(EventSourceName)} must be a valid non-empty string");
            }
        }
    }
}