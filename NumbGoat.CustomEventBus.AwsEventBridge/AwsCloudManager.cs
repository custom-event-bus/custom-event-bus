using Amazon;
using Amazon.EventBridge;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Amazon.SimpleNotificationService;
using Amazon.SQS;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NumbGoat.CustomEventBus.Common.Exceptions;

namespace NumbGoat.CustomEventBus.AwsEventBridge;

public class AwsCloudManager
{
    private readonly ILogger<AwsCloudManager> _logger;
    private readonly AwsEventBridgeOptions _options;
    private readonly RegionEndpoint? _awsRegion;
    private readonly AWSCredentials _awsCredentials;

    public AwsCloudManager(ILogger<AwsCloudManager> logger, IOptions<AwsEventBridgeOptions> options)
    {
        _logger = logger;
        _options = options.Value;
        _options.Validate();
        _awsRegion = _options.AwsRegion != null ? RegionEndpoint.GetBySystemName(_options.AwsRegion) : null;
        if (_options.AwsAccessKey != null)
        {
            _awsCredentials = new BasicAWSCredentials(_options.AwsAccessKey, _options.AwsSecretAccessKey);
        }
        else if (!string.IsNullOrWhiteSpace(_options.AwsProfile))
        {
            var chain = new CredentialProfileStoreChain();
            bool success = chain.TryGetAWSCredentials(_options.AwsProfile, out _awsCredentials);
            if (!success)
            {
                throw new EventBusException(
                    $"Could not get the AWS Credentials for {nameof(AwsEventBridgeProvider)}");
            }
        }
        else
        {
            _awsCredentials = FallbackCredentialsFactory.GetCredentials();
        }
    }

    public AmazonSQSClient GetSqsClient()
    {
        return new AmazonSQSClient(_awsCredentials, _awsRegion);
    }

    public AmazonSimpleNotificationServiceClient GetSnsClient()
    {
        return new AmazonSimpleNotificationServiceClient(_awsCredentials, _awsRegion);
    }

    public AmazonEventBridgeClient GetEventBridgeClient()
    {
        return new AmazonEventBridgeClient(_awsCredentials, _awsRegion);
    }

    public string GetAwsAccountId()
    {
        IAmazonSecurityTokenService stsClient = new AmazonSecurityTokenServiceClient(_awsCredentials, _awsRegion);
        string accountId = stsClient.GetCallerIdentityAsync(new GetCallerIdentityRequest()).Result.Account;
        return accountId;
    }

    public string GetRegionString()
    {
        return _awsRegion?.SystemName ?? throw new NullReferenceException("No AWS Region Configured.");
    }
}