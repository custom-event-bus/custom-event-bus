namespace NumbGoat.CustomEventBus.Common;

public abstract class BaseIncomingMessage<T> : IIncomingMessage
{
    protected BaseIncomingMessage(T? message, string messageId, MessageMetadata metadata)
    {
        Message = message;
        Metadata = metadata;
        MessageId = messageId;
    }

    public T? Message { get; }
    public string MessageId { get; }
    public MessageMetadata Metadata { get; }
}