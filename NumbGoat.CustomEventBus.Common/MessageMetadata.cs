﻿namespace NumbGoat.CustomEventBus.Common;

/// <summary>
///     Metadata for messages
/// </summary>
public readonly struct MessageMetadata
{
    /// <summary>
    ///     Metadata for messages
    /// </summary>
    /// <param name="transportName">The queue or topic name to send message on</param>
    /// <param name="transportType">Which transport type to use</param>
    public MessageMetadata(string transportName, MessageTransportType transportType)
    {
        TransportName = transportName;
        TransportType = transportType;
    }

    /// <summary>The queue or topic name to send message on</summary>
    public string TransportName { get; }

    /// <summary>Which transport type to use</summary>
    public MessageTransportType TransportType { get; }
}