﻿using Google.Protobuf;

namespace NumbGoat.CustomEventBus.Common;

/// <summary>
///     The base for consumers that handle multiple event types.
/// </summary>
public abstract class BaseMultipleConsumer : IConsumer
{
    /// <summary>
    ///     Override this method to receive messages.
    /// </summary>
    /// <param name="message">The message that has been received from the event bus</param>
    /// <param name="metadata">The messages metadata.</param>
    /// <param name="messageId"></param>
    /// <param name="customHeader">Any custom header information that was included with the message.</param>
    public abstract Task OnHandle(IMessage message, MessageMetadata metadata, string? messageId,
        IMessage? customHeader = null);
}