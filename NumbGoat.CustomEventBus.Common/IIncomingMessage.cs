using Google.Protobuf;

namespace NumbGoat.CustomEventBus.Common;

public interface IIncomingMessage
{
    public string MessageId { get; }
    
    public MessageMetadata Metadata { get; }
}