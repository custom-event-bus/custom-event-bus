using Google.Protobuf;

namespace NumbGoat.CustomEventBus.Common;

public record CustomEventBusMessage
{
    /// <summary>
    ///     The main body of the message.
    /// </summary>
    public IMessage Body { get; set; } = null!;

    /// <summary>
    ///     The custom header, if any was provided.
    /// </summary>
    public IMessage? CustomHeader { get; set; } = null;
}