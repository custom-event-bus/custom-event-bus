﻿namespace NumbGoat.CustomEventBus.Common;

public enum MessageTransportType
{
    Queue = 0,
    Topic = 1
}