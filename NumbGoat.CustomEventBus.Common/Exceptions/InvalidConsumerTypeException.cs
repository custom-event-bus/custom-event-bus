﻿namespace NumbGoat.CustomEventBus.Common.Exceptions;

public class InvalidConsumerTypeException : Exception
{
    public InvalidConsumerTypeException(string message) : base(message)
    {
    }
}