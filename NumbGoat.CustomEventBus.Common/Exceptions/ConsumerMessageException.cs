﻿namespace NumbGoat.CustomEventBus.Common.Exceptions;

public class ConsumerMessageException : Exception
{
    public ConsumerMessageException(string message) : base(message)
    {
    }

    public ConsumerMessageException(string message, Exception innerException) : base(message, innerException)
    {
    }
}