﻿namespace NumbGoat.CustomEventBus.Common.Exceptions;

/// <summary>
///     Exception used when a EventBusProvider does not support the requested transport type.
/// </summary>
public class UnsupportedTransportException : Exception
{
    public UnsupportedTransportException(string message) : base(message)
    {
    }

    public UnsupportedTransportException(string message, Exception innerException) : base(message, innerException)
    {
    }
}