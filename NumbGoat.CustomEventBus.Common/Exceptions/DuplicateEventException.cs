﻿namespace NumbGoat.CustomEventBus.Common.Exceptions;

public class DuplicateEventException : Exception
{
    public DuplicateEventException(string message) : base(message)
    {
    }

    public DuplicateEventException(string message, Exception innerException) : base(message, innerException)
    {
    }
}