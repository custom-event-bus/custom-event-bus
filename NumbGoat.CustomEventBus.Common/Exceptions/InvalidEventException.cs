﻿namespace NumbGoat.CustomEventBus.Common.Exceptions;

public class InvalidEventException : Exception
{
    public InvalidEventException(string message) : base(message)
    {
    }

    public InvalidEventException(string message, Exception innerException) : base(message, innerException)
    {
    }
}