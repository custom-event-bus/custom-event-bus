using Google.Protobuf;
using Google.Protobuf.Reflection;
using Google.Protobuf.WellKnownTypes;
using NumbGoat.CustomEventBus.Common.Exceptions;
using NumbGoat.CustomEventBus.Common.Json;
using NumbGoat.CustomEventBus.Common.MessageDefinitions;
using NumbGoat.CustomEventBus.Common.Proto;
using Type = System.Type;

namespace NumbGoat.CustomEventBus.Common;

public static class EventSerialisation
{
    private static TypeRegistry? _cachedRegistry = null;
    private static Guid? _lastCacheTag = null;

    /// <summary>
    ///     Wraps and serialises a CustomEventBus IMessage in the standard wrapper. Event must be registered in <see cref="EventRegistry"/>
    /// </summary>
    /// <param name="outgoingMessage">The message to Serialize</param>
    /// <param name="eventDefinition">The event defintion that was used to Serialize</param>
    /// <returns>The wrapped and serialised message</returns>
    /// <returns>The wrapped and serialised message</returns>
    /// <exception cref="InvalidEventException">The event could not be found in the registry.</exception>
    public static byte[] SerializeToProto(IOutgoingMessage outgoingMessage, EventDefinition? eventDefinition = null)
    {
        EventBusMessage eventBusMessage = PackMessage(outgoingMessage, eventDefinition);
        return EncodeMessageAsBytes(eventBusMessage);
    }

    /// <summary>
    ///     Wraps and serialises a CustomEventBus IMessage in the standard json routable wrapper (See <see cref="RoutableMessage"/>). Event must be registered in <see cref="EventRegistry"/>
    /// </summary>
    /// <param name="outgoingMessage">The message to Serialize</param>
    /// <param name="eventDefinition">The event definition that was used to Serialize</param>
    /// <returns>The wrapped and serialised message</returns>
    /// <exception cref="InvalidEventException">The event could not be found in the registry.</exception>
    public static string SerializeToJson(IOutgoingMessage outgoingMessage, EventDefinition? eventDefinition = null)
    {
        eventDefinition ??= EventRegistry.GetDefinition(outgoingMessage.MessageType);
        if (eventDefinition == null)
        {
            throw new InvalidMessageException("Event is not registered in the EventRegistry");
        }

        EventBusMessage eventBusMessage = PackMessage(outgoingMessage, eventDefinition);
        RoutableMessage routableMessage = new RoutableMessage
        {
            TransportType = eventDefinition.TransportType == MessageTransportType.Queue
                ? TransportType.Queue
                : TransportType.Topic,
            TransportName = eventDefinition.TransportName,
            Message = eventBusMessage
        };

        return EncodeMessageAsJson(routableMessage);
    }

    private static EventBusMessage PackMessage(IOutgoingMessage outgoingMessage,
        EventDefinition? eventDefinition = null)
    {
        eventDefinition ??= EventRegistry.GetDefinition(outgoingMessage.MessageType);

        if (eventDefinition == null)
        {
            throw new InvalidEventException($"Could not find event definition for {outgoingMessage.MessageType}");
        }

        // Wrap message in our standard wrapper
        var eventBusHeader = new EventBusHeader();
        if (outgoingMessage.CustomMetadata != null)
        {
            EventDefinition? headerDefinition = EventRegistry.GetDefinition(outgoingMessage.CustomMetadata.GetType());
            if (headerDefinition == null)
            {
                throw new InvalidEventException("Failed to find event registration for custom metadata");
            }

            eventBusHeader.CustomMetadata = Any.Pack(outgoingMessage.CustomMetadata,
                outgoingMessage.CustomMetadata.Descriptor.FullName);
            eventBusHeader.CustomMetadataType = headerDefinition.TransportName;
        }

        var eventBusMessage = new EventBusMessage
        {
            Header = eventBusHeader,
            Payload = Any.Pack(outgoingMessage.Message, outgoingMessage.Message.Descriptor.FullName)
        };
        return eventBusMessage;
    }


    /// <summary>
    ///     Unwraps the default message structure. Useful if receiving messages outside of CustomEventBus, i.e. in Lambda or
    ///     Azure Function.
    /// </summary>
    /// <param name="incomingMessage">The message that has been received</param>
    /// <param name="eventDefinition">
    ///     The definition to use for the inner message. If null will attempt to find from
    ///     EventRegistry
    /// </param>
    /// <returns></returns>
    /// <exception cref="InvalidEventException"></exception>
    /// <exception cref="NullReferenceException"></exception>
    public static CustomEventBusMessage DeserializeAndUnwrapMessage(IIncomingMessage incomingMessage,
        EventDefinition? eventDefinition = null)
    {
        (Type messageType, _, _) = eventDefinition ??
                                   EventRegistry.GetDefinition(incomingMessage.Metadata.TransportName) ??
                                   throw new InvalidEventException(
                                       $"Could not find event definition for transport {incomingMessage.Metadata.TransportName}");


        // Unwrap our standard wrapper
        EventBusMessage eventBusMessage = ParseMessage(incomingMessage);
        MessageParser parser = EventRegistry.GetEventParser(messageType) ??
                               throw new InvalidEventException($"Could not find parser for {messageType}");
        IMessage? message = parser.ParseFrom(eventBusMessage.Payload.Value);
        if (message == null)
        {
            throw new NullReferenceException($"Could not parse message {messageType}");
        }

        // Load custom header
        EventBusHeader header = eventBusMessage.Header;
        IMessage? customHeader = null;
        if (header.CustomMetadata != null)
        {
            MessageParser? customHeaderParser = EventRegistry.GetEventParser(header.CustomMetadataType);
            if (customHeaderParser == null)
            {
                throw new InvalidEventException("Failed to find parser for custom header");
            }

            customHeader = customHeaderParser?.ParseFrom(header.CustomMetadata.Value);
        }

        return new CustomEventBusMessage
        {
            Body = message,
            CustomHeader = customHeader
        };
    }

    private static EventBusMessage ParseMessage(IIncomingMessage incomingMessage)
    {
        return incomingMessage switch
        {
            IncomingProtoMessage protoMessage => EventBusMessage.Parser.ParseFrom(protoMessage.Message),
            IncomingJsonMessage jsonMessage => GetJsonParser().Parse<RoutableMessage>(jsonMessage.Message).Message,
            _ => throw new InvalidMessageException($"Unknown message type {incomingMessage.GetType()}")
        };
    }

    private static byte[] EncodeMessageAsBytes(IMessage message) => message.ToByteArray();

    private static string EncodeMessageAsJson(IMessage message)
    {
        return GetJsonFormatter().Format(message);
    }

    private static JsonFormatter GetJsonFormatter()
    {
        CacheTypeRegistry();

        return new JsonFormatter(new JsonFormatter.Settings(true, _cachedRegistry));
    }

    private static void CacheTypeRegistry()
    {
        if (_cachedRegistry != null && _lastCacheTag == EventRegistry.CacheTag)
        {
            return;
        }

        IEnumerable<MessageDescriptor> descriptors = EventRegistry.GetDefinitions().SelectMany(r =>
            r.MessageType.GetProperties().Where(p => p.Name == "Descriptor")
                .Select(p => (MessageDescriptor)p.GetValue(null, null)));
        _cachedRegistry = TypeRegistry.FromMessages(descriptors);
        _lastCacheTag = EventRegistry.CacheTag;
    }

    private static JsonParser GetJsonParser()
    {
        CacheTypeRegistry();
        return new JsonParser(new JsonParser.Settings(10, _cachedRegistry));
    }

    public static void ResetCache()
    {
        _cachedRegistry = null;
        _lastCacheTag = null;
    }
}