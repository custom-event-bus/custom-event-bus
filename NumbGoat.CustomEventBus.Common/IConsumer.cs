﻿using Google.Protobuf;

namespace NumbGoat.CustomEventBus.Common;

public interface IConsumer
{
    public Task OnHandle(IMessage message, MessageMetadata metadata, string? messageId, IMessage? customHeader = null);
}

public interface IConsumer<T> : IConsumer where T : class, IMessage
{
    public Task OnHandle(T message, MessageMetadata metadata, string? messageId, IMessage? customHeader = null);
}