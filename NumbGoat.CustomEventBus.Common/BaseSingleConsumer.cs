﻿using Google.Protobuf;

namespace NumbGoat.CustomEventBus.Common;

/// <summary>
///     The base for most IConsumers, implements some helper function to handle correctly typing messages etc.
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class BaseSingleConsumer<T> : IConsumer<T> where T : class, IMessage
{
    /// <summary>
    ///     Override this method to receive messages.
    /// </summary>
    /// <param name="message">The message that has been received from the event bus</param>
    /// <param name="metadata">The messages metadata.</param>
    /// <param name="messageId"></param>
    /// <param name="customHeader">Any custom header information that was included with the message.</param>
    public abstract Task OnHandle(T message, MessageMetadata metadata, string? messageId,
        IMessage? customHeader = null);

    public virtual async Task OnHandle(IMessage message, MessageMetadata metadata, string? messageId,
        IMessage? customHeader = null)
    {
        if (message is not T typed)
        {
            throw new InvalidCastException($"Consumer can only handle {typeof(T).Name}");
        }

        await OnHandle(typed, metadata, messageId, customHeader);
    }
}