namespace NumbGoat.CustomEventBus.Common.Attributes;

/// <summary>
/// Marks this handler as handling this event.
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
public class EventHandlerAttribute : Attribute
{
    public Type? EventType { get; }

    public EventHandlerAttribute(Type? eventType = null)
    {
        EventType = eventType;
    }
}