﻿namespace NumbGoat.CustomEventBus.Common;

public class EventDefinition
{
    public EventDefinition(Type MessageType, string TransportName, MessageTransportType TransportType)
    {
        this.MessageType = MessageType;
        this.TransportName = TransportName;
        this.TransportType = TransportType;
    }

    public Type MessageType { get; }
    public string TransportName { get; }
    public MessageTransportType TransportType { get; }

    public void Deconstruct(out Type MessageType, out string TransportName, out MessageTransportType TransportType)
    {
        MessageType = this.MessageType;
        TransportName = this.TransportName;
        TransportType = this.TransportType;
    }
}