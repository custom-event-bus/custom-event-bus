using Google.Protobuf;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common.Helpers;

namespace NumbGoat.CustomEventBus.Common.Json;

public abstract class BaseJsonEventBusProvider : BaseEventBusProvider
{
    protected BaseJsonEventBusProvider(ILogger<BaseEventBusProvider> logger) : base(logger)
    {
    }

    public override Task PublishToQueue(IOutgoingMessage message)
    {
        return PublishToQueue(new JsonMessage(message.Metadata, message.ToJson(),message.CustomMetadata));
    }

    public override Task PublishToTopic(IOutgoingMessage message)
    {
        return PublishToTopic(new JsonMessage(message.Metadata, message.ToJson(),message.CustomMetadata));
    }

    /// <summary>
    /// Publish a message to a queue as json. 
    /// </summary>
    protected abstract Task PublishToQueue(JsonMessage message);

    /// <summary>
    /// Publish a message to a topic as protobuf bytes. 
    /// </summary>
    protected abstract Task PublishToTopic(JsonMessage message);
}