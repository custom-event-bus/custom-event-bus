namespace NumbGoat.CustomEventBus.Common.Json;

public class IncomingJsonMessage : BaseIncomingMessage<string>
{
    public IncomingJsonMessage(string? message, string messageId, MessageMetadata metadata) : base(message, messageId,
        metadata)
    {
    }
}