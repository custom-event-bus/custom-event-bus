using Google.Protobuf;

namespace NumbGoat.CustomEventBus.Common.Json;

public record JsonMessage(MessageMetadata Metadata, string MessageJson, IMessage? CustomMetadata);