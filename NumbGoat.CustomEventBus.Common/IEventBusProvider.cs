﻿namespace NumbGoat.CustomEventBus.Common;

public interface IEventBusProvider : IDisposable
{
    public bool IsReady { get; }
    public bool HasOutgoingMessages { get; }

    public Task Start(IEnumerable<EventDefinition> eventTypes,
        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> registeredConsumers,
        CancellationToken stoppingToken);

    public bool SupportsQueues();
    public bool SupportsTopics();


    public  Task SubscribeToTopics(Func<IIncomingMessage, Task> handler);

    public Task SubscribeToQueues(Func<IIncomingMessage, Task> handler);
    
    public Task RemoveTopicSubscription();
    public Task RemoveQueueSubscription();
    
    public Task PublishToQueue(IOutgoingMessage message);
    public Task PublishToTopic(IOutgoingMessage message);
}