using Microsoft.Extensions.Logging;

namespace NumbGoat.CustomEventBus.Common;

/// <summary>
/// Base class for most EventBus providers.
/// </summary>
public abstract class BaseEventBusProvider : IEventBusProvider
{
    private readonly ILogger<BaseEventBusProvider> _logger;

    protected Func<IIncomingMessage, Task>? TopicHandler { get; private set; }
    protected Func<IIncomingMessage, Task>? QueueHandler { get; private set; }

    protected BaseEventBusProvider(ILogger<BaseEventBusProvider> logger)
    {
        _logger = logger;
    }

    public virtual void Dispose()
    {
    }

    /// <summary>
    /// True if this provider is currently able to send messages.
    /// </summary>
    public abstract bool IsReady { get; }

    /// <summary>
    /// True if this provider still has messages in their pending queue.
    /// </summary>
    public abstract bool HasOutgoingMessages { get; }

    /// <summary>
    /// Called on startup by CustomEventBus
    /// </summary>
    /// <param name="eventTypes">A list of currently registered event types.</param>
    /// <param name="registeredConsumers">The consumers registered to the EventBus at startup. Map of event type to consumer types</param>
    /// <param name="stoppingToken">Token used to stop the provider once cancelled.
    /// By default you have 60 seconds to finish in flight requests once cancelled.</param>
    /// <returns></returns>
    public virtual Task Start(IEnumerable<EventDefinition> eventTypes,
        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> registeredConsumers, CancellationToken stoppingToken)
    {
        return Task.CompletedTask;
    }

    /// <summary>
    /// Return true if the provider supports queue transports.
    /// </summary>
    public abstract bool SupportsQueues();

    /// <summary>
    /// Return true if the provider supports topic transports.
    /// </summary>
    /// <returns></returns>
    public abstract bool SupportsTopics();

    public abstract Task PublishToQueue(IOutgoingMessage message);

    public abstract Task PublishToTopic(IOutgoingMessage message);

    public virtual Task SubscribeToTopics(Func<IIncomingMessage, Task> handler)
    {
        TopicHandler = handler;
        return Task.CompletedTask;
    }

    public virtual Task SubscribeToQueues(Func<IIncomingMessage, Task> handler)
    {
        QueueHandler = handler;
        return Task.CompletedTask;
    }

    public virtual Task RemoveTopicSubscription()
    {
        TopicHandler = null;
        return Task.CompletedTask;
    }

    public virtual Task RemoveQueueSubscription()
    {
        QueueHandler = null;
        return Task.CompletedTask;
    }

    public async Task InvokeSubscriber(IIncomingMessage message)
    {
        switch (message.Metadata.TransportType)
        {
            case MessageTransportType.Queue:
                if (QueueHandler == null)
                {
                    await OnMessageComplete(message);
                    return;
                }

                try
                {
                    await QueueHandler.Invoke(message);
                    await OnMessageComplete(message);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, $"");
                    await OnMessageFailure(message);
                }

                break;
            case MessageTransportType.Topic:
                if (TopicHandler == null)
                {
                    await OnMessageComplete(message);
                    return;
                }

                try
                {
                    await TopicHandler.Invoke(message);
                    await OnMessageComplete(message);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, $"");
                    await OnMessageFailure(message);
                }

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    protected abstract Task OnMessageComplete(IIncomingMessage completedMessage);

    protected abstract Task OnMessageFailure(IIncomingMessage failedMessage);
}