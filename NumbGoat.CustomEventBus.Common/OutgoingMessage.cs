using Google.Protobuf;
using NumbGoat.CustomEventBus.Common.MessageDefinitions;

namespace NumbGoat.CustomEventBus.Common;

public interface IOutgoingMessage
{
    /// <summary>
    /// The metadata for this message that determined its routing.
    /// </summary>
    public MessageMetadata Metadata { get; }

    /// <summary>
    /// The message header to influence routing on sending. Header is already serialised in <see cref="Message"/>
    /// </summary>
    public IMessage? CustomMetadata { get; }

    /// <summary>
    /// The body of a outgoing message
    /// </summary>
    public IMessage Message { get; }

    /// <summary>
    /// Gets the messages type.
    /// </summary>
    public Type MessageType { get; }
}

public class OutgoingMessage : IOutgoingMessage
{
    public OutgoingMessage(IMessage message, MessageMetadata metadata, IMessage? customMetadata = null)
    {
        Message = message;
        Metadata = metadata;
        CustomMetadata = customMetadata;
    }

    public MessageMetadata Metadata { get; }

    public IMessage? CustomMetadata { get; }
    public IMessage Message { get; }

    public virtual Type MessageType => Message.GetType();
}

public class OutgoingMessage<T> : OutgoingMessage where T : class, IMessage
{
    public OutgoingMessage(T message, MessageMetadata metadata, EventBusHeader header) : base(message, metadata, header)
    {
    }

    /// <summary>
    /// Gets the typed message
    /// </summary>
    /// <returns></returns>
    public T? GetMessage() => Message as T;

    public override Type MessageType => typeof(T);
}