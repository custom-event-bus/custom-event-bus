﻿namespace NumbGoat.CustomEventBus.Common.Proto;

public class IncomingProtoMessage : BaseIncomingMessage<byte[]>
{
    public IncomingProtoMessage(byte[] message, string messageId,MessageMetadata metadata) : base(message, messageId,metadata)
    {
    }
}