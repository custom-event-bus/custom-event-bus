using Google.Protobuf;

namespace NumbGoat.CustomEventBus.Common.Proto;
public record ProtoMessage(MessageMetadata Metadata, byte[] MessageBytes, IMessage? CustomMetadata);