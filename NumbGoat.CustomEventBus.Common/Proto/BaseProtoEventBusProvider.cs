using Google.Protobuf;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common.Helpers;

namespace NumbGoat.CustomEventBus.Common.Proto;

public abstract class BaseProtoEventBusProvider : BaseEventBusProvider
{
    protected BaseProtoEventBusProvider(ILogger<BaseEventBusProvider> logger) : base(logger)
    {
    }

    public override Task PublishToQueue(IOutgoingMessage message)
    {
        return PublishToQueue(new ProtoMessage(message.Metadata, message.ToProto(), message.CustomMetadata));
    }

    public override Task PublishToTopic(IOutgoingMessage message)
    {
        return PublishToTopic(new ProtoMessage(message.Metadata, message.ToProto(), message.CustomMetadata));
    }

    /// <summary>
    /// Publish a message to a queue as protobuf bytes. 
    /// </summary>
    protected abstract Task PublishToQueue(ProtoMessage protoMessage);

    /// <summary>
    /// Publish a message to a topic as protobuf bytes. 
    /// </summary>
    protected abstract Task PublishToTopic(ProtoMessage protoMessage);
}