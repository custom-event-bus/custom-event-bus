namespace NumbGoat.CustomEventBus.Common.Helpers;

public static class MessageExtensions
{
    /// <inheritdoc cref="EventSerialisation.SerializeToJson"/>
    public static string ToJson(this IOutgoingMessage message, EventDefinition? eventDefinition = null)
    {
        return EventSerialisation.SerializeToJson(message, eventDefinition);
    }

    /// <inheritdoc cref="EventSerialisation.SerializeToProto"/>
    public static byte[] ToProto(this IOutgoingMessage message, EventDefinition? eventDefinition = null)
    {
        return EventSerialisation.SerializeToProto(message, eventDefinition);
    }

    public static MessageMetadata ToMetadata(this EventDefinition eventDefinition)
    {
        return new MessageMetadata(eventDefinition.TransportName, eventDefinition.TransportType);
    }
}