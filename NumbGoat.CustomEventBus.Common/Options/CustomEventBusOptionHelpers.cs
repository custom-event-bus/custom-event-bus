namespace NumbGoat.CustomEventBus.Common.Options;

public static class CustomEventBusOptionHelpers
{
    /// <summary>
    ///     Adds a consumer registration mapping to the options object. Consumer is still required to be registered in DI.
    /// </summary>
    /// <param name="configuration"></param>
    /// <param name="eventType">The type of event to pass to the consumer</param>
    /// <param name="consumerType">The consumer that will handle the event</param>
    public static void AddConsumerRegistration(this CustomEventBusConfiguration configuration, Type eventType,
        Type consumerType)
    {
        EventConsumerMapping? existingItem =
            configuration.ConsumerEventTypeMappings.SingleOrDefault(mapping => mapping.EventType == eventType);
        if (existingItem != null)
        {
            if (!existingItem.ConsumerTypes.Contains(consumerType))
            {
                existingItem.ConsumerTypes.Add(consumerType);
            }
        }
        else
        {
            configuration.ConsumerEventTypeMappings.Add(new EventConsumerMapping
            {
                EventType = eventType,
                ConsumerTypes = new HashSet<Type>
                {
                    consumerType
                }
            });
        }
    }
}