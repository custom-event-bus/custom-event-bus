using System.Reflection;

namespace NumbGoat.CustomEventBus.Common.Options;

public class CustomEventBusConfiguration
{
    /// <summary>
    ///     Registrations of consumers to message types.
    /// </summary>
    public readonly List<EventConsumerMapping> ConsumerEventTypeMappings = new();

    /// <summary>
    /// List of assemblies to search for consumers in.
    /// </summary>
    public List<Assembly> ConsumerAssemblies = new();

    /// <summary>
    /// Should CustomEventBus load handlers based on their attributes, where they can be found in assemblies.
    /// </summary>
    public bool AutoLoadHandlers { get; set; } = true;
}