namespace NumbGoat.CustomEventBus.Common.Options;

public record EventConsumerMapping
{
    /// <summary>
    ///     The type of event which should be passed to the consumers
    /// </summary>
    public Type EventType { get; set; } = null!;

    /// <summary>
    ///     The consumers that wish to receive this event.
    /// </summary>
    public HashSet<Type> ConsumerTypes { get; set; } = new();
}