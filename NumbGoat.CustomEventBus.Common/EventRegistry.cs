﻿using System.Collections.Concurrent;
using System.Reflection;
using System.Text.RegularExpressions;
using Google.Protobuf;
using NumbGoat.CustomEventBus.Common.Exceptions;

namespace NumbGoat.CustomEventBus.Common;

public static class EventRegistry
{
    private static readonly ConcurrentDictionary<Type, MessageParser> Parsers = new();

    private static HashSet<EventDefinition> Registry { get; } = new();

    public static Guid CacheTag { get; private set; } = Guid.NewGuid();

    /// <summary>
    ///     Adds all the events it can find that are in the provided assembly, with the namespace prefix provided.
    /// </summary>
    public static void AddAllEvents(string eventNamespacePrefix, Assembly assembly,
        string? transportNamePrefix = null)
    {
        AddAllEvents(eventNamespacePrefix, new[] { assembly }, transportNamePrefix);
        UpdateCacheTag();
    }

    /// <summary>
    ///     Adds all the events it can find that are in the provided assemblies, with the namespace prefix provided.
    /// </summary>
    public static void AddAllEvents(string eventNamespacePrefix, IEnumerable<Assembly> assemblies,
        string? transportNamePrefix = null)
    {
        IEnumerable<Type> eventTypes = assemblies
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => !type.IsAbstract && type is { Namespace: not null, IsPublic: true } &&
                           typeof(IMessage).IsAssignableFrom(type) &&
                           type.Namespace.StartsWith(eventNamespacePrefix) && !type.Name.Contains('<'))
            .ToList();

        foreach (Type eventType in eventTypes)
        {
            RegisterEvent(transportNamePrefix, eventType);
        }
        UpdateCacheTag();
    }

    public static void RegisterEvent(string? transportNamePrefix, Type eventType)
    {
        string eventName = eventType.Name;

        // Check event name is valid.
        if (eventName.IndexOfAny(new[] { '.', '?' }) >= 0)
        {
            throw new InvalidEventException($"Event name {eventName} contained invalid characters.");
        }

        string transportName = GenerateTransportName(eventName, transportNamePrefix);
        if (RegistryContainsEvent(transportName) || RegistryContainsEventType(eventType))
            // Skip events that have been manually defined.
        {
            return;
        }

        // Find event operation from name.
        MessageTransportType eventT =
            eventName.EndsWith("Job") ? MessageTransportType.Queue : MessageTransportType.Topic;

        var definition = new EventDefinition(eventType, transportName, eventT);
        RegisterEvent(definition);
        UpdateCacheTag();
    }

    /// <summary>
    ///     Registers a new event. The event must be assignable from IMessage.
    /// </summary>
    /// <param name="eventDefinition">The event definition to use</param>
    /// <exception cref="DuplicateEventException">A event with this transport name or type has already been registered.</exception>
    /// <exception cref="InvalidEventException">Event does not implement IMessage</exception>
    public static void RegisterEvent(EventDefinition eventDefinition)
    {
        if (RegistryContainsEvent(eventDefinition.TransportName))
        {
            throw new DuplicateEventException(
                $"Event with transport name {eventDefinition.TransportName} already registered");
        }

        if (!typeof(IMessage).IsAssignableFrom(eventDefinition.MessageType))
        {
            throw new InvalidEventException("Events must implement from IMessage");
        }

        Registry.Add(eventDefinition);
        UpdateCacheTag();
    }

    public static void Remove(Type eventType)
    {
        Registry.RemoveWhere(ed => ed.MessageType == eventType);
        UpdateCacheTag();
    }

    public static MessageParser<T>? GetEventParser<T>() where T : IMessage<T>
    {
        MessageParser? parser = GetEventParser(typeof(T));
        return parser as MessageParser<T>;
    }

    public static MessageParser? GetEventParser(Type eventType)
    {
        if (Parsers.TryGetValue(eventType, out MessageParser? eventParser))
        {
            return eventParser;
        }

        if (!typeof(IMessage).IsAssignableFrom(eventType))
        {
            throw new InvalidEventException($"{eventType} is not a IMessage");
        }

        PropertyInfo? prop = eventType.GetProperty("Parser", BindingFlags.Public | BindingFlags.Static);
        if (prop == null)
        {
            throw new InvalidEventException($"{eventType} did not have a property for Parser");
        }

        if (prop.GetValue(null, null) is not MessageParser parser)
        {
            throw new Exception("Parser was not a MessageParser");
        }

        Parsers.TryAdd(eventType, parser);
        return parser;
    }

    public static MessageParser? GetEventParser(string transportName)
    {
        EventDefinition type = Registry.Single(ed =>
            string.Equals(transportName, ed.TransportName, StringComparison.OrdinalIgnoreCase));
        return GetEventParser(type.MessageType);
    }

    /// <summary>
    ///     Gets all of the registered EventDefinitions
    /// </summary>
    public static IEnumerable<EventDefinition> GetDefinitions()
    {
        return new List<EventDefinition>(Registry);
    }

    /// <summary>
    ///     Gets enumerator for registered events.
    /// </summary>
    public static IEnumerator<EventDefinition> GetEnumerator()
    {
        return Registry.GetEnumerator();
    }

    /// <summary>
    ///     Checks if there are any events registered for this type.
    /// </summary>
    /// <param name="eventType">The type of the event.</param>
    /// <returns>True if a event exists, otherwise false.</returns>
    public static bool RegistryContainsEvent(Type eventType)
    {
        return Registry != null && Registry.Any(d => d.MessageType == eventType);
    }

    /// <summary>
    ///     Checks if the registry contains an event that uses this transport name.
    /// </summary>
    /// <param name="transportName">Transport name to check.</param>
    /// <returns></returns>
    public static bool RegistryContainsEvent(string transportName)
    {
        if (Registry == null)
        {
            return false;
        }

        return Registry.Any(definition =>
            string.Equals(definition.TransportName, transportName, StringComparison.OrdinalIgnoreCase));
    }

    /// <summary>
    ///     Checks if the registry contains an event matching this type.
    /// </summary>
    /// <param name="eventType"></param>
    /// <returns></returns>
    public static bool RegistryContainsEventType(Type eventType)
    {
        return Registry != null && Registry.Any(definition => definition.MessageType == eventType);
    }

    /// <summary>
    ///     Get the definition for this type.
    /// </summary>
    /// <param name="eventType">The type to check if there is a event.</param>
    /// <returns>The event definition or null.</returns>
    public static EventDefinition? GetDefinition(Type eventType)
    {
        return Registry.FirstOrDefault(d => d.MessageType == eventType);
    }

    public static EventDefinition? GetDefinition(string transportName)
    {
        return Registry.FirstOrDefault(d => d.TransportName == transportName);
    }

    /// <summary>
    ///     Takes a upper camel case (LikeThisSortOfThing) and generates a azure safe hyphen seperated name
    ///     (like-this-sort-of-thing)
    /// </summary>
    /// <param name="className">The string of the class name to transform</param>
    /// <returns>kebab-case of the name.</returns>
    private static string GenerateTransportName(string className, string? transportPrefix = null)
    {
        if (string.IsNullOrWhiteSpace(transportPrefix))
        {
            transportPrefix = "";
        }

        // This regex splits on upper case letters.
        List<string> split = Regex.Split(className, @"(?<!^)(?=[A-Z])").ToList();
        // Put the list back together, lower cased and with '-'s
        var name = $"{transportPrefix}{split.Select(s => s.ToLower()).Aggregate((s, s1) => s + '-' + s1)}";
        return name;
    }

    private static void UpdateCacheTag()
    {
        CacheTag = Guid.NewGuid();
    }

    /// <summary>
    ///     Clears the registry and marks it as not initialized.
    /// </summary>
    public static void Clear()
    {
        Registry?.Clear();
        UpdateCacheTag();
    }
}