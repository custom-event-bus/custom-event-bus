﻿using System.Collections.Concurrent;
using Google.Protobuf;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Json;

namespace NumbGoat.CustomEventBus;

public class InMemoryJsonProvider : BaseJsonEventBusProvider
{
    /// <summary>
    ///     In memory queue to mimic a queue in a proper provider
    /// </summary>
    private readonly ConcurrentDictionary<string, ConcurrentQueue<string>> _queues = new();

    /// <summary>
    ///     The thread we are processing sending events on.
    /// </summary>
    private Thread? _processThread;

    public InMemoryJsonProvider(ILogger<InMemoryProtoProvider> logger) : base(logger)
    {
    }

    public override bool IsReady => _isReady;
    private bool _isReady = false;
    public override bool HasOutgoingMessages => _queues.Any(q => !q.Value.IsEmpty);

    public override bool SupportsQueues()
    {
        return true;
    }

    public override bool SupportsTopics()
    {
        return true;
    }

    protected override Task OnMessageComplete(IIncomingMessage completedProtoMessage)
    {
        return Task.CompletedTask;
    }

    protected override Task OnMessageFailure(IIncomingMessage failedProtoMessage)
    {
        return Task.CompletedTask;
    }

    public override void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public override Task Start(IEnumerable<EventDefinition> messageTypes,
        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> registeredConsumers, CancellationToken stoppingToken)
    {
        base.Start(messageTypes, registeredConsumers, stoppingToken);

        _processThread = new Thread(ProcessWrapper);
        _processThread.Start();
        _isReady = true;
        return Task.CompletedTask;

        void ProcessWrapper()
        {
            Task processTask = ProcessQueues(stoppingToken);
            processTask.Wait(CancellationToken.None);
        }
    }

    private async Task ProcessQueues(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            foreach (KeyValuePair<string, ConcurrentQueue<string>> pair in _queues)
            {
                string? queueName = pair.Key;
                ConcurrentQueue<string>? queue = pair.Value;

                if (queue?.IsEmpty ?? true)
                {
                    continue;
                }

                bool success = queue.TryDequeue(out string? message);
                if (!success || message == null)
                {
                    continue;
                }

                await InvokeSubscriber(new IncomingJsonMessage(message, new Guid().ToString(), new MessageMetadata(
                    queueName, MessageTransportType.Queue)));
            }

            Thread.Sleep(50);
        }
    }

    protected override Task PublishToQueue(JsonMessage message)
    {
        if (!_queues.ContainsKey(message.Metadata.TransportName))
        {
            _queues[message.Metadata.TransportName] = new ConcurrentQueue<string>();
        }

        _queues[message.Metadata.TransportName].Enqueue(message.MessageJson);
        return Task.CompletedTask;
    }

    protected override async Task PublishToTopic(JsonMessage message)
    {
        await InvokeSubscriber(new IncomingJsonMessage(message.MessageJson, new Guid().ToString(),
            new MessageMetadata(message.Metadata.TransportName, MessageTransportType.Topic)));
    }
}