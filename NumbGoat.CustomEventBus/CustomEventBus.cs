﻿using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Reflection;
using Google.Protobuf;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Attributes;
using NumbGoat.CustomEventBus.Common.Exceptions;
using NumbGoat.CustomEventBus.Common.Helpers;
using NumbGoat.CustomEventBus.Common.Options;
using NumbGoat.CustomEventBus.Common.Proto;
using Type = System.Type;

namespace NumbGoat.CustomEventBus;

/// <summary>
///     A customisable event bus that uses one of many common event / messaging systems to provide a simple event bus like
///     construct.
/// </summary>
public class CustomEventBus : IHostedService, IDisposable
{
    /// <summary>
    ///     Cancellation token to stop the providers
    /// </summary>
    private readonly CancellationTokenSource _cancellationTokenSource = new();

    /// <summary>
    ///     Our current configuration
    /// </summary>
    // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
    private readonly CustomEventBusConfiguration _configuration;

    /// <summary>
    ///     Our list of consumers, and a list of the events they are listening to.
    ///     Map of message type to consumer
    /// </summary>
    private readonly ConcurrentDictionary<Type, List<Type>> _consumers = new();

    private readonly ILogger<CustomEventBus> _logger;

    /// <summary>
    ///     The Event Bus Provider that is registered to handle queues.
    /// </summary>
    private readonly List<IEventBusProvider> _queueProviders;

    private readonly IServiceProvider _serviceProvider;

    /// <summary>
    ///     The Event Bus Provider that is registered to handle topics.
    /// </summary>
    private readonly List<IEventBusProvider> _topicProviders;

    /// <summary>
    ///     The main thread handling the running of CustomEventBus
    /// </summary>
    private Thread? _runThread;

    public CustomEventBus(ILogger<CustomEventBus> logger, CustomEventBusConfiguration configuration,
        IServiceProvider serviceProvider, IEnumerable<IEventBusProvider> providers)
    {
        _logger = logger;
        _configuration = configuration;
        _serviceProvider = serviceProvider;
        IEnumerable<IEventBusProvider> eventBusProviders = providers.ToList();
        _queueProviders = eventBusProviders.Where(provider => provider.SupportsQueues()).ToList();
        _topicProviders = eventBusProviders.Where(provider => provider.SupportsTopics()).ToList();
    }

    public bool IsAcceptingNewMessages { get; private set; } = true;


    /// <summary>
    ///     True if the EventBus is still in the process of sending messages
    /// </summary>
    public bool HasOutgoingMessages => _queueProviders.Any(p => p.HasOutgoingMessages) ||
                                       _topicProviders.Any(p => p.HasOutgoingMessages);

    /// <summary>
    ///     True if the EventBus is ready to send messages.
    ///     No need to check before calling Publish, this will start working once the event bus is ready.
    /// </summary>
    public bool IsReady => _queueProviders.All(p => p.IsReady) && _topicProviders.All(p => p.IsReady);

    public void Dispose()
    {
        _queueProviders.ForEach(p => p?.Dispose());
        _topicProviders.ForEach(p => p?.Dispose());
        GC.SuppressFinalize(this);
    }

    public Task StartAsync(CancellationToken stoppingToken)
    {
        void RunWrapper()
        {
            Task runTask = Run(_cancellationTokenSource.Token);
            runTask.Wait(stoppingToken);
        }

        _runThread = new Thread(RunWrapper);
        _runThread.Start();
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        IsAcceptingNewMessages = false;
        _cancellationTokenSource.Cancel();
        SpinWait.SpinUntil(() => !HasOutgoingMessages, 10000);
        if (HasOutgoingMessages)
        {
            _logger.LogError("EventBus still had pending messages on shutdown");
        }

        return Task.CompletedTask;
    }

    /// <summary>
    ///     Publish a new message on the event bus
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <param name="customMetadata">Any extra data to send with the message, packed into the header.</param>
    /// <exception cref="NullReferenceException">Event definition could not be found.</exception>
    public async Task Publish(IMessage? message, IMessage? customMetadata = null)
    {
        if (!IsAcceptingNewMessages)
        {
            throw new EventBusException("Event bus is not accepting new messages, likely in shutdown");
        }

        if (message == null)
        {
            return;
        }

        if (!IsReady)
        {
            SpinWait.SpinUntil(() => IsReady, 5000);
            if (!IsReady)
            {
                throw new EventBusException("Providers never became ready");
            }
        }

        var eventDefinition = EventRegistry.GetDefinition(message.GetType());
        if (eventDefinition == null)
        {
            throw new NullReferenceException(nameof(eventDefinition));
        }

        var outgoing = new OutgoingMessage(message, eventDefinition.ToMetadata(), customMetadata);

        _logger.LogInformation("Publishing message {MessageId} to {TransportType} {TransportName}",
            message.GetType().Name,
            outgoing.Metadata.TransportType.ToString(), outgoing.Metadata.TransportName);
        switch (eventDefinition.TransportType)
        {
            case MessageTransportType.Queue:
                foreach (IEventBusProvider queueProvider in _queueProviders)
                {
                    await queueProvider.PublishToQueue(outgoing);
                }

                break;
            case MessageTransportType.Topic:
                foreach (IEventBusProvider topicProvider in _topicProviders)
                {
                    await topicProvider.PublishToQueue(outgoing);
                }

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(eventDefinition.TransportType));
        }
    }


    /// <summary>
    ///     Registers a type of consumer with a type of message. Is additive with previous calls.
    /// </summary>
    /// <param name="messageType">The message type, must be an IMessage</param>
    /// <param name="consumerType">The consumers type, must be an IConsumer</param>
    /// <exception cref="InvalidConsumerTypeException">Thrown if message or consumer types are wrong</exception>
    public void RegisterConsumer(Type messageType, Type consumerType)
    {
        if (!typeof(IMessage).IsAssignableFrom(messageType))
        {
            throw new InvalidConsumerTypeException($"{messageType} is not an IMessage.");
        }

        if (!typeof(IConsumer).IsAssignableFrom(consumerType))
        {
            throw new InvalidConsumerTypeException($"{consumerType} is not an IConsumer.");
        }

        if (!_consumers.ContainsKey(messageType))
        {
            _consumers[messageType] = new List<Type>();
        }

        _logger.LogDebug("Consumer {Consumer} registered for event type {EventType}", consumerType, messageType);
        _consumers[messageType].Add(consumerType);
    }

    /// <summary>
    ///     Registers a message type with a consumer type. Will add this to list of existing consumers.
    /// </summary>
    /// <typeparam name="TMessage">The message type</typeparam>
    /// <typeparam name="TConsumer">The type of consumer to handle it</typeparam>
    public void RegisterConsumer<TMessage, TConsumer>() where TConsumer : IConsumer where TMessage : IMessage
    {
        RegisterConsumer(typeof(TMessage), typeof(TConsumer));
    }

    /// <summary>
    ///     This is run as part of the background thread for the service.
    /// </summary>
    private async Task Run(CancellationToken stoppingToken)
    {
        // Auto consumers
        var autoMappedConsumers = new List<EventConsumerMapping>();

        if (_configuration.AutoLoadHandlers)
        {
            if (_configuration.ConsumerAssemblies.Count == 0)
            {
                // If no assemblies have been manually provided then we should load all the types in the current execution tree.
                if (Assembly.GetEntryAssembly() != null)
                {
                    List<AssemblyName> assemblies = Assembly.GetEntryAssembly()?.GetReferencedAssemblies().ToList() ??
                                                    new List<AssemblyName>();
                    AssemblyName? entryAssemblyName = Assembly.GetEntryAssembly()?.GetName();
                    if (entryAssemblyName != null)
                    {
                        assemblies.Add(entryAssemblyName);
                    }

                    _configuration.ConsumerAssemblies.AddRange(assemblies.Select(Assembly.Load));
                }
            }

            // Map all the consumers based on the properties from their attributes.
            IEnumerable<Type> consumersWithAttribute = _configuration.ConsumerAssemblies.SelectMany(a =>
                a.GetExportedTypes().Where(t =>
                    t.IsClass && !t.IsAbstract &&
                    t.GetCustomAttributes<EventHandlerAttribute>(true).Any()
                    && typeof(IConsumer).IsAssignableFrom(t)
                ));
            foreach (Type consumerType in consumersWithAttribute)
            {
                IEnumerable<EventHandlerAttribute> attributes =
                    consumerType.GetCustomAttributes<EventHandlerAttribute>();
                foreach (EventHandlerAttribute attribute in attributes)
                {
                    if (attribute.EventType == null)
                    {
                        // Get the IConsumer<> interface and get its generic type
                        var interfaceType = consumerType.GetInterfaces()
                            .FirstOrDefault(i => i.GetGenericTypeDefinition() == typeof(IConsumer<>));
                        if (interfaceType == null)
                        {
                            throw new EventBusException(
                                "Handler attribute on type other then IConsumer<> must provide a type");
                        }

                        // Check if this is a single consumer and map the type from its generic type arguments.
                        var genericType = interfaceType.GetGenericArguments().First();
                        if (genericType == null || !typeof(IMessage).IsAssignableFrom(genericType))
                        {
                            throw new EventBusException($"Generic type was invalid for {consumerType}");
                        }

                        autoMappedConsumers.Add(new EventConsumerMapping
                        {
                            EventType = genericType,
                            ConsumerTypes = new HashSet<Type>(new[] { consumerType })
                        });
                        continue;
                    }

                    autoMappedConsumers.Add(new EventConsumerMapping
                    {
                        EventType = attribute.EventType,
                        ConsumerTypes = new HashSet<Type>(new[] { consumerType })
                    });
                }
            }
        }

        var combinedConsumerMaps = new List<EventConsumerMapping>(_configuration.ConsumerEventTypeMappings);
        combinedConsumerMaps.AddRange(autoMappedConsumers);

        // Manual consumers
        foreach (EventConsumerMapping? mapping in combinedConsumerMaps)
        {
            foreach (Type consumerType in mapping.ConsumerTypes)
            {
                RegisterConsumer(mapping.EventType, consumerType);
            }
        }

        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> consumers =
            _consumers.Select(pair => new KeyValuePair<Type, IEnumerable<Type>>(pair.Key, pair.Value.ToList()))
                .ToList();

        List<EventDefinition> eventDefinitions = EventRegistry.GetDefinitions().ToList();

        // Start all providers that are in both lists.
        List<IEventBusProvider> dualProviders = _queueProviders.Intersect(_topicProviders).ToList();
        foreach (IEventBusProvider? provider in dualProviders)
        {
            await provider.Start(eventDefinitions, consumers, stoppingToken);
        }

        foreach (IEventBusProvider? queueProvider in _queueProviders.Where(p => !dualProviders.Contains(p)))
        {
            await queueProvider.Start(eventDefinitions.Where(e => e.TransportType == MessageTransportType.Queue),
                consumers, stoppingToken);
        }

        foreach (IEventBusProvider? topicProvider in _topicProviders.Where(p => !dualProviders.Contains(p)))
        {
            await topicProvider.Start(eventDefinitions.Where(e => e.TransportType == MessageTransportType.Topic),
                consumers, stoppingToken);
        }

        await RegisterNewMessageEvents();
    }

    /// <summary>
    ///     Add the buses event handler to the providers.
    /// </summary>
    /// <returns></returns>
    private async Task RegisterNewMessageEvents()
    {
        foreach (IEventBusProvider queueProvider in _queueProviders)
        {
            await queueProvider.SubscribeToQueues(HandleMessageFromProvider);
        }

        foreach (IEventBusProvider topicProvider in _topicProviders)
        {
            await topicProvider.SubscribeToTopics(HandleMessageFromProvider);
        }
    }


    /// <summary>
    ///     Hande sending messages to the consumers. This is also responsible for deserializing the message.
    /// </summary>
    private Task HandleMessageFromProvider(IIncomingMessage incomingMessage)
    {
        if (string.IsNullOrWhiteSpace(incomingMessage.Metadata.TransportName))
        {
            throw new ArgumentException(
                $"TransportName {incomingMessage.Metadata.TransportName} is not a valid transport");
        }

        EventDefinition definition = EventRegistry.GetDefinition(incomingMessage.Metadata.TransportName) ??
                                     throw new InvalidEventException(
                                         $"Failed to get definition for event {incomingMessage.Metadata.TransportName}");
        if (!_consumers.ContainsKey(definition.MessageType))
        {
            return Task.CompletedTask;
        }

        CustomEventBusMessage message =
            EventSerialisation.DeserializeAndUnwrapMessage(incomingMessage, definition);
        _logger.LogTrace("Received message {MessageId} from {TransportType} {TransportName}",
            incomingMessage.MessageId,
            incomingMessage.Metadata.TransportType.ToString(), incomingMessage.Metadata.TransportName);

        List<Type> consumers = _consumers[definition.MessageType];
        IServiceScope scopedProvider = _serviceProvider.CreateScope();

        // Go through each consumer and provide them with the message.
        var consumerTasks = new List<Task>();
        foreach (Type consumerType in consumers.Where(consumerType => typeof(IConsumer).IsAssignableFrom(consumerType)))
        {
            if (scopedProvider.ServiceProvider.GetRequiredService(consumerType) is not IConsumer consumer)
            {
                throw new NullReferenceException($"Failed to find consumer for type {consumerType}");
            }

            Task consumerTask = consumer.OnHandle(message.Body, incomingMessage.Metadata, incomingMessage.MessageId,
                message.CustomHeader);
            consumerTasks.Add(consumerTask);
        }

        Task waitForConsumers = Task.WhenAll(consumerTasks);
        waitForConsumers.Wait(TimeSpan.FromMinutes(2));

        if (consumerTasks.Any(ct => ct.IsFaulted))
        {
            IEnumerable<Exception> exceptions = consumerTasks.Where(t => t.IsFaulted).SelectMany(t =>
                t.Exception?.InnerExceptions ?? new ReadOnlyCollection<Exception>(new List<Exception>()));
            _logger.LogError(new AggregateException(exceptions),
                "One or more event handlers had uncaught exceptions");
        }

        return Task.CompletedTask;
    }
}