﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NumbGoat.CustomEventBus.Common.Options;

namespace NumbGoat.CustomEventBus;

public static class CustomEventBusExtensions
{
    /// <summary>
    ///     Adds custom event bus to the services, and as a hosted service as required.
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="configure"></param>
    public static void AddCustomEventBus(this IServiceCollection serviceCollection,
        Action<CustomEventBusConfiguration>? configure = null)
    {
        var configuration = new CustomEventBusConfiguration();
        configure?.Invoke(configuration);
        serviceCollection.TryAddSingleton(provider =>
            ActivatorUtilities.CreateInstance<CustomEventBus>(provider, configuration));
        serviceCollection.AddHostedService(provider => provider.GetRequiredService<CustomEventBus>());
    }
}