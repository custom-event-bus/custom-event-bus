﻿using System.Collections.Concurrent;
using Google.Protobuf;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Proto;

namespace NumbGoat.CustomEventBus;

public class InMemoryProtoProvider : BaseProtoEventBusProvider
{
    /// <summary>
    ///     In memory queue to mimic a queue in a proper provider
    /// </summary>
    private readonly ConcurrentDictionary<string, ConcurrentQueue<byte[]>> _queues = new();

    /// <summary>
    ///     The thread we are processing sending events on.
    /// </summary>
    private Thread? _processThread;

    public InMemoryProtoProvider(ILogger<InMemoryProtoProvider> logger) : base(logger)
    {
    }

    public override bool IsReady => _isReady;
    private bool _isReady = false;
    public override bool HasOutgoingMessages => _queues.Any(q => !q.Value.IsEmpty);

    public override bool SupportsQueues()
    {
        return true;
    }

    public override bool SupportsTopics()
    {
        return true;
    }

    protected override Task OnMessageComplete(IIncomingMessage completedProtoMessage)
    {
        return Task.CompletedTask;
    }

    protected override Task OnMessageFailure(IIncomingMessage failedProtoMessage)
    {
        return Task.CompletedTask;
    }

    public override void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public override Task Start(IEnumerable<EventDefinition> messageTypes,
        IEnumerable<KeyValuePair<Type, IEnumerable<Type>>> registeredConsumers, CancellationToken stoppingToken)
    {
        base.Start(messageTypes, registeredConsumers, stoppingToken);

        _processThread = new Thread(ProcessWrapper);
        _processThread.Start();
        _isReady = true;
        return Task.CompletedTask;

        void ProcessWrapper()
        {
            Task processTask = ProcessQueues(stoppingToken);
            processTask.Wait(CancellationToken.None);
        }
    }

    private async Task ProcessQueues(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            foreach (KeyValuePair<string, ConcurrentQueue<byte[]>> pair in _queues)
            {
                string? queueName = pair.Key;
                ConcurrentQueue<byte[]>? queue = pair.Value;

                if (queue?.IsEmpty ?? true)
                {
                    continue;
                }

                bool success = queue.TryDequeue(out byte[]? message);
                if (!success || message == null)
                {
                    continue;
                }

                await InvokeSubscriber(new IncomingProtoMessage(message, new Guid().ToString(), new MessageMetadata(
                    queueName, MessageTransportType.Queue)));
            }

            Thread.Sleep(50);
        }
    }

    protected override Task PublishToQueue(ProtoMessage protoMessage)
    {
        (MessageMetadata metadata, byte[] messageBytes, _) = protoMessage; 
        if (!_queues.ContainsKey(metadata.TransportName))
        {
            _queues[metadata.TransportName] = new ConcurrentQueue<byte[]>();
        }

        _queues[metadata.TransportName].Enqueue(messageBytes);
        return Task.CompletedTask;
    }

    protected override async Task PublishToTopic(ProtoMessage protoMessage)
    {
        await InvokeSubscriber(new IncomingProtoMessage(protoMessage.MessageBytes, new Guid().ToString(),
            new MessageMetadata(protoMessage.Metadata.TransportName, MessageTransportType.Topic)));
    }
}