﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Options;
using NumbGoat.CustomEventBus.Tests.Common;
using NumbGoat.CustomEventBus.Tests.Common.Consumers;
using NumbGoat.CustomEventBus.Tests.Protos;
using Xunit;
using Xunit.Abstractions;
using Xunit.DependencyInjection.Logging;

namespace NumbGoat.CustomEventBus.Tests;

public class CustomEventBusTests : IAsyncLifetime
{
    private readonly ILogger<CustomEventBusTests> _logger;
    private readonly ITestOutputHelper _outputHelper;
    private IHost? _host;

    public CustomEventBusTests(ILogger<CustomEventBusTests> logger, ITestOutputHelper outputHelper)
    {
        _logger = logger;
        _outputHelper = outputHelper;
    }

    public Task InitializeAsync()
    {
        HostHelpers.ResetAndRegisterTestEvents();
        return Task.CompletedTask;
    }

    public async Task DisposeAsync()
    {
        if (_host != null)
        {
            var bus = _host.Services.GetService<CustomEventBus>();
            await _host.StopAsync();
            if (bus != null)
            {
                SpinWait.SpinUntil(() => !bus.HasOutgoingMessages, 10000);
            }

            _host.Dispose();
            
            _host = null;
        }

        EventRegistry.Clear();
    }

    [Fact]
    public async Task TestProtoEventSend_Success_00()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer testEventConsumer) = await GetNewProtoEventBus();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestEventSend_Success_00"
        };
        await customEventBus.Publish(testEvent);
        SpinWait.SpinUntil(() => testEventConsumer.Count > 0, 4000);
        testEventConsumer.Count.Should().Be(1);
        testEventConsumer.ReceivedMessages.Should().Contain(s => s == testEvent.MessageText);
    }

    [Fact]
    public async Task TestProtoEventSend_Success_01()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer testEventConsumer) = await GetNewProtoEventBus();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestEventSend_Success_01"
        };
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        SpinWait.SpinUntil(() => testEventConsumer.Count == 3, 4000);
        testEventConsumer.Count.Should().Be(3);
        testEventConsumer.ReceivedMessages.Should().Contain(s => s == testEvent.MessageText);
        testEventConsumer.ReceivedMessages.Count.Should().Be(3);
    }

    /// <summary>
    /// Test with custom header.
    /// </summary>
    [Fact]
    public async Task TestProtoEventSend_Success_02()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer testEventConsumer) = await GetNewProtoEventBus();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestJsonEventSend_Success_02"
        };

        var customHeader = new BasicTestCustomHeader
        {
            SomeText = "Header text"
        };

        await customEventBus.Publish(testEvent, customHeader);

        SpinWait.SpinUntil(() => testEventConsumer.Count > 0, 4000);

        testEventConsumer.Count.Should().Be(1);
        testEventConsumer.ReceivedMessages.Should().Contain(s => s == testEvent.MessageText);
        testEventConsumer.ReceivedMessages.Count.Should().Be(1);

        testEventConsumer.HeaderCount.Should().Be(1);
        testEventConsumer.CustomHeaders.Should().Contain(s => ((BasicTestCustomHeader)s).SomeText == "Header text");
        testEventConsumer.CustomHeaders.Count.Should().Be(1);
    }

    [Fact]
    public async Task TestProtoAutoEventSend_Success_01()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer testEventConsumer,
                TestAutoEventConsumer autoEventConsumer, TestAutoMultiEventConsumer multiEventConsumer) =
            await GetNewProtoEventBusAllConsumers();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestProtoAutoEventSend_Success_01"
        };
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        SpinWait.SpinUntil(() => testEventConsumer.Count == 3, 4000);

        autoEventConsumer.Count.Should().Be(3);
        autoEventConsumer.ReceivedMessages.Should().Contain(s => s == testEvent.MessageText);
        autoEventConsumer.ReceivedMessages.Count.Should().Be(3);
    }

    [Fact]
    public async Task TestProtoAutoMultiEventSend_Success_01()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer _,
                TestAutoEventConsumer _, TestAutoMultiEventConsumer multiEventConsumer) =
            await GetNewProtoEventBusAllConsumers();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestProtoAutoEventSend_Success_01"
        };
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        SpinWait.SpinUntil(() => multiEventConsumer.CountOne == 3, 4000);

        multiEventConsumer.CountOne.Should().Be(3);
        multiEventConsumer.ReceivedMessagesOne.Should().Contain(s => s == testEvent.MessageText);
        multiEventConsumer.ReceivedMessagesOne.Count.Should().Be(3);
    }

    [Fact]
    public async Task TestProtoAutoMultiEventSend_Success_02()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer _,
                TestAutoEventConsumer _, TestAutoMultiEventConsumer multiEventConsumer) =
            await GetNewProtoEventBusAllConsumers();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestProtoAutoEventSend_Success_01"
        };

        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        SpinWait.SpinUntil(() => multiEventConsumer.CountOne == 3, 4000);


        var testEventTwo = new BasicTestEventTwo
        {
            MessageText = "TestProtoAutoEventSendTwo_Success_01"
        };
        await customEventBus.Publish(testEventTwo);
        await customEventBus.Publish(testEventTwo);
        SpinWait.SpinUntil(() => multiEventConsumer.CountTwo == 2, 4000);


        multiEventConsumer.CountOne.Should().Be(3);
        multiEventConsumer.ReceivedMessagesOne.Should().Contain(s => s == testEvent.MessageText);
        multiEventConsumer.ReceivedMessagesOne.Count.Should().Be(3);

        multiEventConsumer.CountTwo.Should().Be(2);
        multiEventConsumer.ReceivedMessagesTwo.Should().Contain(s => s == testEventTwo.MessageText);
        multiEventConsumer.ReceivedMessagesTwo.Count.Should().Be(2);
    }

    [Fact]
    public async Task TestJsonEventSend_Success_00()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer testEventConsumer) = await GetNewJsonEventBus();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestEventSend_Success_00"
        };
        await customEventBus.Publish(testEvent);
        SpinWait.SpinUntil(() => testEventConsumer.Count > 0, 4000);
        testEventConsumer.Count.Should().Be(1);
        testEventConsumer.ReceivedMessages.Should().Contain(s => s == testEvent.MessageText);
    }

    [Fact]
    public async Task TestJsonEventSend_Success_01()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer testEventConsumer) = await GetNewJsonEventBus();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestEventSend_Success_01"
        };
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        await customEventBus.Publish(testEvent);
        SpinWait.SpinUntil(() => testEventConsumer.Count == 3, 4000);
        testEventConsumer.Count.Should().Be(3);
        testEventConsumer.ReceivedMessages.Should().Contain(s => s == testEvent.MessageText);
        testEventConsumer.ReceivedMessages.Count.Should().Be(3);
    }

    /// <summary>
    /// Test with custom header.
    /// </summary>
    [Fact]
    public async Task TestJsonEventSend_Success_02()
    {
        (CustomEventBus customEventBus, TestSingleEventConsumer testEventConsumer) = await GetNewJsonEventBus();

        var testEvent = new BasicTestEvent
        {
            MessageText = "TestJsonEventSend_Success_02"
        };

        var customHeader = new BasicTestCustomHeader
        {
            SomeText = "Header text"
        };

        await customEventBus.Publish(testEvent, customHeader);

        SpinWait.SpinUntil(() => testEventConsumer.Count > 0, 4000);

        testEventConsumer.Count.Should().Be(1);
        testEventConsumer.ReceivedMessages.Should().Contain(s => s == testEvent.MessageText);
        testEventConsumer.ReceivedMessages.Count.Should().Be(1);

        testEventConsumer.HeaderCount.Should().Be(1);
        testEventConsumer.CustomHeaders.Should().Contain(s => ((BasicTestCustomHeader)s).SomeText == "Header text");
        testEventConsumer.CustomHeaders.Count.Should().Be(1);
    }

    private async Task<(CustomEventBus, TestSingleEventConsumer)> GetNewProtoEventBus()
    {
        (CustomEventBus? customEventBus, TestSingleEventConsumer? testSingleEventConsumer, _, _) =
            await GetNewProtoEventBusAllConsumers();
        return (customEventBus, testSingleEventConsumer);
    }

    private async Task<(CustomEventBus, TestSingleEventConsumer, TestAutoEventConsumer, TestAutoMultiEventConsumer)>
        GetNewProtoEventBusAllConsumers()
    {
        (IHost host, CustomEventBus customEventBus) =
            await HostHelpers.CreateBasicEventBus(new List<Type> { typeof(InMemoryProtoProvider) },
                new List<EventConsumerMapping>
                {
                    new()
                    {
                        EventType = typeof(BasicTestEvent),
                        ConsumerTypes = new HashSet<Type>(new[] { typeof(TestSingleEventConsumer) })
                    }
                }, outputHelper: _outputHelper,
                configure: configuration => { configuration.AutoLoadHandlers = true; });
        await HostHelpers.StartEventBusAndWaitForReady(host);
        _host = host;
        return (customEventBus, _host.Services.GetRequiredService<TestSingleEventConsumer>(),
            _host.Services.GetRequiredService<TestAutoEventConsumer>(),
            _host.Services.GetRequiredService<TestAutoMultiEventConsumer>());
    }

    private async Task<(CustomEventBus, TestSingleEventConsumer)> GetNewJsonEventBus()
    {
        (IHost host, CustomEventBus customEventBus) =
            await HostHelpers.CreateBasicEventBus(new List<Type> { typeof(InMemoryJsonProvider) },
                new List<EventConsumerMapping>
                {
                    new()
                    {
                        EventType = typeof(BasicTestEvent),
                        ConsumerTypes = new HashSet<Type>(new[] { typeof(TestSingleEventConsumer) })
                    }
                }, outputHelper: _outputHelper,
                configure: configuration => { configuration.AutoLoadHandlers = true; });
        await HostHelpers.StartEventBusAndWaitForReady(host);
        _host = host;
        return (customEventBus, _host.Services.GetRequiredService<TestSingleEventConsumer>());
    }
}