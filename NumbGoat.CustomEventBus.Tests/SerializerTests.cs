using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Helpers;
using NumbGoat.CustomEventBus.Tests.Common;
using NumbGoat.CustomEventBus.Tests.Protos;
using Xunit;

namespace NumbGoat.CustomEventBus.Tests;

public class SerializerTests : IAsyncLifetime
{
    public Task InitializeAsync()
    {
        HostHelpers.ResetAndRegisterTestEvents();
        return Task.CompletedTask;
    }

    public Task DisposeAsync()
    {
        EventRegistry.Clear();
        return Task.CompletedTask;
    }

    [Fact]
    public void TestJsonEvent_Success_00()
    {
        var basicEvent = new BasicTestEvent
        {
            MessageText = "Hello Json"
        };
        var eventDefinition = EventRegistry.GetDefinition(basicEvent.GetType());
        if (eventDefinition == null)
        {
            throw new Exception("No event definition found");
        }

        var eventAsJson =
            EventSerialisation.SerializeToJson(new OutgoingMessage(basicEvent, eventDefinition.ToMetadata()));
        eventAsJson.Should().NotBeNull();
        eventAsJson.Should().NotBeEmpty();
        eventAsJson.Should().Contain(basicEvent.MessageText);
    }

    [Fact]
    public void TestJsonEvent_Success_01()
    {
        var basicEvent = new BasicTestEvent
        {
            MessageText = "Hello Json"
        };
        var customHeader = new BasicTestCustomHeader
        {
            SomeText = "Header text is fun!"
        };
        var eventDefinition = EventRegistry.GetDefinition(basicEvent.GetType());
        if (eventDefinition == null)
        {
            throw new Exception("No event definition found");
        }

        var eventAsJson =
            EventSerialisation.SerializeToJson(
                new OutgoingMessage(basicEvent, eventDefinition.ToMetadata(), customHeader), eventDefinition);
        eventAsJson.Should().NotBeNull();
        eventAsJson.Should().NotBeEmpty();
        eventAsJson.Should().Contain(basicEvent.MessageText);
        eventAsJson.Should().Contain(customHeader.SomeText);
    }
}