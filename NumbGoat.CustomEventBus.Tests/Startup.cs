﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.Options;
using NumbGoat.CustomEventBus.Tests.Common.Consumers;
using NumbGoat.CustomEventBus.Tests.Protos;
using Serilog;
using Xunit;
using Xunit.DependencyInjection;
using Xunit.DependencyInjection.Logging;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace NumbGoat.CustomEventBus.Tests;

public class Startup
{
    public IHostBuilder CreateHostBuilder()
    {
        if (Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") == null)
        {
            Environment.SetEnvironmentVariable("DOTNET_ENVIRONMENT", "Development");
        }

        return Host.CreateDefaultBuilder().ConfigureLogging(ConfigureLogging).ConfigureServices(ConfigureServices);
    }

    private void ConfigureLogging(HostBuilderContext context, ILoggingBuilder builder)
    {
        builder.AddSerilog();
    }

    private void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        services.AddLogging(lb => lb.AddXunitOutput());
    }
}