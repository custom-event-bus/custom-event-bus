using System;
using System.Reflection;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Common.MessageDefinitions;
using NumbGoat.CustomEventBus.Tests.Protos;
using Xunit;

namespace NumbGoat.CustomEventBus.Tests;

public class EventRegistryTests : IAsyncLifetime
{
    private readonly ILogger<EventRegistryTests> _logger;
    private readonly IServiceProvider _serviceProvider;

    public EventRegistryTests(ILogger<EventRegistryTests> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
    }

    public Task InitializeAsync()
    {
        return Task.CompletedTask;
    }

    public Task DisposeAsync()
    {
        EventRegistry.Clear();
        return Task.CompletedTask;
    }

    [Fact]
    public void RegisterOneEvent_Success_00()
    {
        const string transportName = "basic-test-event";
        // Register an event
        EventRegistry.RegisterEvent(new EventDefinition(typeof(BasicTestEvent), transportName,
            MessageTransportType.Queue));

        // Get and check registration
        EventDefinition? definitionFromType = EventRegistry.GetDefinition(typeof(BasicTestEvent));
        definitionFromType.Should().NotBeNull();
        definitionFromType!.MessageType.Should().Be(typeof(BasicTestEvent));
        definitionFromType.TransportType.Should().Be(MessageTransportType.Queue);
        definitionFromType.TransportName.Should().Be(transportName);
    }

    [Fact]
    public void RegisterAllEvents_Success_00()
    {
        // Register events
        EventRegistry.AddAllEvents("NumbGoat.CustomEventBus.Tests.Protos",
            Assembly.Load("NumbGoat.CustomEventBus.Tests.Protos"));

        // Get and check registration
        EventDefinition? definitionFromType = EventRegistry.GetDefinition(typeof(BasicTestEvent));
        definitionFromType.Should().NotBeNull();
        definitionFromType!.MessageType.Should().Be(typeof(BasicTestEvent));
        definitionFromType.TransportType.Should()
            .Be(MessageTransportType.Topic); // Event registry loads as Topic unless class has job in the title.
    }

    [Fact]
    public void RegisterAllEvents_UnknownEvent_00()
    {
        // Register events
        EventRegistry.AddAllEvents("NumbGoat.CustomEventBus.Tests.Protos",
            Assembly.Load("NumbGoat.CustomEventBus.Tests.Protos"));

        // Get and check registration
        EventDefinition?
            definitionFromType =
                EventRegistry.GetDefinition(typeof(EventBusHeader)); // This event shouldn't be registered
        definitionFromType.Should().BeNull();
    }

    [Fact]
    public void TransportPrefix_Success_00()
    {
        // Register events
        EventRegistry.AddAllEvents("NumbGoat.CustomEventBus.Tests.Protos",
            Assembly.Load("NumbGoat.CustomEventBus.Tests.Protos"), "gen1-");

        // Get and check registration
        EventDefinition? definitionFromType = EventRegistry.GetDefinition(typeof(BasicTestEvent));
        definitionFromType.Should().NotBeNull();
        definitionFromType!.MessageType.Should().Be(typeof(BasicTestEvent));
        definitionFromType.TransportType.Should()
            .Be(MessageTransportType.Topic); // Event registry loads as Topic unless class has job in the title.
        definitionFromType.TransportName.Should().StartWith("gen1-");
    }
}