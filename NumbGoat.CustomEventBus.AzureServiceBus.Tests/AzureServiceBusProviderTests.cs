using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NumbGoat.CustomEventBus.AzureServiceBus.Options;
using NumbGoat.CustomEventBus.Common;
using NumbGoat.CustomEventBus.Tests.Common;
using NumbGoat.CustomEventBus.Tests.Common.Consumers;
using NumbGoat.CustomEventBus.Tests.Protos;
using Xunit;
using Xunit.Abstractions;

namespace NumbGoat.CustomEventBus.AzureServiceBus.Tests;

public class AzureServiceBusProviderTests : IAsyncLifetime
{
    private const string TempSubscriptionName = "unit-testing-temp";
    private readonly ILogger<AzureServiceBusProviderTests> _logger;
    private readonly IServiceProvider _serviceProvider;
    private readonly IConfiguration _configuration;
    private readonly ITestOutputHelper _outputHelper;
    private CancellationTokenSource? _cts;
    private AzureServiceBusProvider? _provider;
    private IHost? _host;

    public AzureServiceBusProviderTests(ILogger<AzureServiceBusProviderTests> logger, IServiceProvider serviceProvider,
        IConfiguration configuration, ITestOutputHelper outputHelper)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
        _configuration = configuration;
        _outputHelper = outputHelper;
    }

    public Task InitializeAsync()
    {
        _cts = new CancellationTokenSource();
        HostHelpers.ResetAndRegisterTestEvents();
        return Task.CompletedTask;
    }

    public async Task DisposeAsync()
    {
        _cts?.Cancel();
        if (_provider != null)
        {
            await _provider.RemoveSubscription("basic-test-event", TempSubscriptionName);
            await _provider.ClearTransport("basic-test-event", "unit-testing");
        }

        if (_host != null)
        {
            await _host.StopAsync();
        }
    }

    [MemberNotNullWhen(true, nameof(_provider))]
    private async Task<(CustomEventBus, AzureServiceBusProvider, TestSingleEventConsumer)> CreateEventBus()
    {
        (IHost host, CustomEventBus customEventBus) =
            await HostHelpers.CreateBasicEventBus(new[] { typeof(AzureServiceBusProvider) },
                (context, collection) =>
                {
                    collection.Configure<AzureServiceBusProviderOptions>(_configuration.GetSection("AzureServiceBus"));
                }, _outputHelper, configure: configuration => { configuration.AutoLoadHandlers = true; });
        _host = host;
        IEventBusProvider? foundProvider = host.Services.GetServices<IEventBusProvider>()
            .FirstOrDefault(p => p.GetType().IsAssignableTo(typeof(AzureServiceBusProvider)));
        _provider = foundProvider as AzureServiceBusProvider ??
                    throw new Exception("Could not get AzureServiceBus provider");
        var consumer = host.Services.GetRequiredService<TestSingleEventConsumer>();
        return (customEventBus, _provider, consumer);
    }

    private async Task<(CustomEventBus, AzureServiceBusProvider, TestAutoEventConsumer)> CreateAndStartEventBus()
    {
        (IHost host, CustomEventBus customEventBus) =
            await HostHelpers.CreateBasicEventBus(new[] { typeof(AzureServiceBusProvider) },
                (context, collection) =>
                {
                    collection.Configure<AzureServiceBusProviderOptions>(_configuration.GetSection("AzureServiceBus"));
                }, _outputHelper, configure: configuration => { configuration.AutoLoadHandlers = true; });
        _host = host;
        IEventBusProvider? foundProvider = host.Services.GetServices<IEventBusProvider>()
            .FirstOrDefault(p => p.GetType().IsAssignableTo(typeof(AzureServiceBusProvider)));
        _provider = foundProvider as AzureServiceBusProvider ??
                    throw new Exception("Could not get AzureServiceBus provider");
        var consumer = host.Services.GetRequiredService<TestAutoEventConsumer>();
        await HostHelpers.StartEventBusAndWaitForReady(host);
        return (customEventBus, _provider, consumer);
    }

    [Fact]
    private async Task Constructor_Success_00()
    {
        (_, AzureServiceBusProvider provider, _) = await CreateEventBus();
        provider.Should().NotBeNull();
    }

    [Fact]
    private async Task Connect_Success_00()
    {
        (_, AzureServiceBusProvider provider, _) = await CreateEventBus();
        await provider.Start(ArraySegment<EventDefinition>.Empty, Array.Empty<KeyValuePair<Type, IEnumerable<Type>>>(),
            _cts!.Token);
        SpinWait.SpinUntil(() => _provider is { IsReady: true }, TimeSpan.FromSeconds(30));
        _provider!.IsReady.Should().BeTrue(); // _provider isnt null if CreateEventBus() returns properly
    }

    [Fact]
    private async Task SendQueueEvent_Success_00()
    {
        // Prepare test event 
        (_, AzureServiceBusProvider provider, _) = await CreateEventBus();
        EventDefinition? eventDef = EventRegistry.GetDefinition(typeof(BasicTestEvent));
        eventDef.Should().NotBeNull();
        var eventDefinitions = new List<EventDefinition>
        {
            eventDef!
        };
        await provider.Start(eventDefinitions, Array.Empty<KeyValuePair<Type, IEnumerable<Type>>>(), _cts!.Token);

        // Create and queue event
        var testMetadata = new BasicTestMetadata
        {
            Version = "v1.0.0"
        };
        var testEvent = new BasicTestEvent
        {
            MessageText = "This is a test :)"
        };
        var outgoingMessage = new OutgoingMessage(testEvent,
            new MessageMetadata(eventDef!.TransportName, eventDef.TransportType), testMetadata);
        await provider.PublishToQueue(outgoingMessage);

        SpinWait.SpinUntil(() => !provider.HasOutgoingMessages, 10000);
        provider.HasOutgoingMessages.Should().BeFalse();
    }

    [Fact]
    public async Task ProcessEvent_Success_00()
    {
        (CustomEventBus customEventBus, AzureServiceBusProvider provider, TestSingleEventConsumer testCounterConsumer) =
            await CreateAndStartEventBus();


        // Create and queue event
        var testMetadata = new BasicTestMetadata
        {
            Version = "v1.0.0"
        };
        var testEvent = new BasicTestEvent
        {
            MessageText = "This is a test :)"
        };
        await customEventBus.Publish(testEvent, testMetadata);
        _logger.LogInformation("Waiting for CustomEventBus to publish event.");
        SpinWait.SpinUntil(() => !customEventBus.HasOutgoingMessages || _cts!.IsCancellationRequested,
            TimeSpan.FromMinutes(2));
        customEventBus.HasOutgoingMessages.Should().BeFalse();
        _logger.LogInformation("Waiting for consumer to receive event.");
        SpinWait.SpinUntil(() => testCounterConsumer.Count >= 1, TimeSpan.FromSeconds(30));
        Thread.Sleep(2000); // To catch the case where an event get received twice, which should fail.
        testCounterConsumer.Count.Should().Be(1);
    }

    [Fact]
    public async Task ProcessMultipleEvents_Success_00()
    {
        (CustomEventBus customEventBus, AzureServiceBusProvider provider, TestSingleEventConsumer testCounterConsumer) =
            await CreateAndStartEventBus();

        // Create and queue event
        var testMetadata = new BasicTestMetadata
        {
            Version = "v1.0.0"
        };
        var testEvent = new BasicTestEvent
        {
            MessageText = "This is a test :)"
        };
        var testEvent1 = new BasicTestEvent
        {
            MessageText = "This is the second part :("
        };
        await customEventBus.Publish(testEvent, testMetadata);
        await customEventBus.Publish(testEvent1, testMetadata);
        _logger.LogInformation("Waiting for CustomEventBus to publish event.");
        SpinWait.SpinUntil(() => !customEventBus.HasOutgoingMessages || _cts!.IsCancellationRequested,
            TimeSpan.FromMinutes(2));
        customEventBus.HasOutgoingMessages.Should().BeFalse();
        _logger.LogInformation("Waiting for consumer to receive event.");
        SpinWait.SpinUntil(() => testCounterConsumer.Count >= 2, TimeSpan.FromSeconds(30));
        Thread.Sleep(2000); // To catch the case where an event get received more than twice, which should fail.
        testCounterConsumer.Count.Should().Be(2);
    }

    [Fact]
    public async Task ProcessMultipleEvents_Success_01()
    {
        (CustomEventBus customEventBus, AzureServiceBusProvider provider, TestSingleEventConsumer testCounterConsumer) =
            await CreateAndStartEventBus();

        // Create and queue event
        var testMetadata = new BasicTestMetadata
        {
            Version = "v1.0.0"
        };
        var testMetadata2 = new BasicTestMetadata
        {
            Version = "v1.0.1"
        };
        var testEvent = new BasicTestEvent
        {
            MessageText = "This is a test :)"
        };
        var testEvent1 = new BasicTestEvent
        {
            MessageText = "This is the second part :("
        };
        await customEventBus.Publish(testEvent, testMetadata);
        await customEventBus.Publish(testEvent1, testMetadata2);
        _logger.LogInformation("Waiting for CustomEventBus to publish event.");
        SpinWait.SpinUntil(() => !customEventBus.HasOutgoingMessages || _cts!.IsCancellationRequested,
            TimeSpan.FromMinutes(2));
        customEventBus.HasOutgoingMessages.Should().BeFalse();
        _logger.LogInformation("Waiting for consumer to receive event.");
        SpinWait.SpinUntil(() => testCounterConsumer.Count >= 2, TimeSpan.FromSeconds(30));
        Thread.Sleep(2000); // To catch the case where an event get received more than twice, which should fail.
        testCounterConsumer.Count.Should().Be(2);
    }


    [Fact]
    public async Task EnsureSubscriptionExists_Success_00()
    {
        (_, AzureServiceBusProvider provider, _) = await CreateEventBus();
        await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName);
    }

    [Fact]
    public async Task EnsureSubscriptionExistsTwice_Success_00()
    {
        (_, AzureServiceBusProvider provider, _) = await CreateEventBus();
        await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName);
        await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName);
    }

    [Fact]
    public async Task EnsureSubscriptionInParallel_Success_00()
    {
        (_, AzureServiceBusProvider provider, _) = await CreateEventBus();
        var tasks = new List<Task>()
        {
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
            Task.Run(async () => await provider.EnsureSubscriptionExists("basic-test-event", TempSubscriptionName)),
        };

        await Task.WhenAll(tasks);

        IEnumerable<Task> failedTasks = tasks.Where(t => !t.IsCompletedSuccessfully);
        failedTasks.Should().BeEmpty("One (or more) of the parallel EnsureSubscriptionExists failed");
    }
}