﻿using Xunit;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Xunit.DependencyInjection.Logging;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace NumbGoat.CustomEventBus.AzureServiceBus.Tests;

public class Startup
{
    public IHostBuilder CreateHostBuilder()
    {
        if (Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") == null)
        {
            Environment.SetEnvironmentVariable("DOTNET_ENVIRONMENT", "Development");
        }

        return Host.CreateDefaultBuilder().ConfigureLogging(ConfigureLogging).ConfigureServices(ConfigureServices);
    }

    private void ConfigureLogging(HostBuilderContext context, ILoggingBuilder builder)
    {
        LoggerConfiguration? logConfig = new LoggerConfiguration()
            .WriteTo.Console()
            .MinimumLevel.Verbose();
        builder.AddSerilog(logConfig.CreateLogger());
    }

    private void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        services.AddLogging(lb => lb.AddXunitOutput());
    }
}